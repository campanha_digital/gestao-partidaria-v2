<?php
//
/* place */
// require_once("bootstrap.php");


// begin: ajuste 04042017
require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/Database.php';
/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');

/* DOCTRINE ***************************************************************/
// end: ajuste 04042017


if (!$user) $user = $_GET['user'];

if ( !empty($_GET['id']) ) {
	$q = Doctrine_Query::create()
	        ->from('Reclamacoes')
	        ->where('aprovada = "1" AND id = ?', $_GET['id']);
	        
	$Reclamacao = $q->fetchOne();
	
} else { die('Objeto não encontrado'); }

//Votos
$q = Doctrine_Query::create()
        ->from('Votos')
        ->where('reclamacao_id = ?', $Reclamacao->id);

$Votos = $q->execute();

$votosCount = count($Votos);
$podeVotar = '1';
foreach ($Votos as $voto) {
	if ($voto->usuario_id == $user) $podeVotar = '0';
}
if (!$user) $podeVotar = '0';

//Comments
$q = Doctrine_Query::create()
        ->from('Comentarios')
        ->where('reclamacao_id = ?', $Reclamacao->id)
        ->where('aprovado = ?', 1);

$Comentarios = $q->execute();


$img = $Reclamacao->ilustracao_url;
if($Reclamacao->ilustracao_url_depois){
	$img_depois = $Reclamacao->ilustracao_url_depois;
}

if($img == '')
	$img = $img_depois;
$video = "";

if ($Reclamacao->ilustracao_tipo == 'video') {

	$lastpart = strstr($img,'&');
	$videoId = str_replace(array('http://www.youtube.com/watch?v=','https://www.youtube.com/watch?v=',$lastpart),'',$img);		
	
	$img = 'https://i.ytimg.com/vi/'.$videoId.'/hqdefault.jpg';
	$videoEmbed = 'https://www.youtube.com/embed/'.$videoId;
	$videoTag = '<meta property="og:video"  content="'.$img.'" />';
}



$matriz = array('message' => $appPostMessage,
 'name'  => $Reclamacao->titulo,
 'caption' => $Reclamacao->endereco,
 'description' => str_replace("\r", "", str_replace("\n", "", $Reclamacao->descricao)),
 'link'  => $appBaseUrl."?app_data=".$Reclamacao->id,
 'picture'    => $img,
 'user_message' => $appSharePrompt
);

echo '<script>window.open("https://www.facebook.com/sharer/sharer.php?u=https://governodigital.net.br/app/'.$appName.'/?app_data='.$_GET['id'].'", "pop", "width=600, height=400, scrollbars=no");</script>';
echo '';

echo json_encode($matriz);