<?php
/* load */
require_once "user.php";

$q = Doctrine_Query::create()
        ->from('Reclamacoes')
        ->where('id = ?',$_POST['id']);

$Reclamacoes = $q->execute();

$matriz = array();
foreach ($Reclamacoes as $reclamacao) {
	
	$img = $reclamacao->ilustracao_url;
	$info_arquivo = explode("/",$img);
	$tot = count($info_arquivo);
	$info_arquivo = $info_arquivo[$tot - 1];

	$img_depois = $reclamacao->ilustracao_url_depois;
	$info_arquivo_depois = explode("/",$img_depois);
	$tot = count($info_arquivo_depois);
	$info_arquivo_depois = $info_arquivo_depois[$tot - 1];

	$img_principal = $reclamacao->ilustracao_url_principal;
	
	if ($reclamacao->ilustracao_tipo == 'video') {
	
		$lastpart = strstr($img,'&');
		$videoId = str_replace(array("http://www.youtube.com/watch?v=","https://www.youtube.com/watch?v=",$lastpart),"",$img);
		
		$img = "https://i.ytimg.com/vi/".$videoId."/hqdefault.jpg";		
	}
	
	if($Usuario->is_admin){
		$is_admin = 1;
	} else {
		$is_admin = null;
	}

	if(strstr($info_arquivo,"GoogleStreet") !== false){
		$info_arquivo = 'GoogleStreetView.jpg';
	}
	if(strstr($info_arquivo_depois,"GoogleStreet") !== false){
		$info_arquivo_depois = 'GoogleStreetView.jpg';
	}

	$matriz = array('id'        =>$reclamacao->id,
			  'latitude'  =>$reclamacao->latitude, 
			  'longitude' =>$reclamacao->longitude, 
			  'fbpicture' =>"https://graph.facebook.com/".$reclamacao->usuario_id."/picture",
			  'endereco'  =>$reclamacao->endereco,
			  'titulo'    =>$reclamacao->titulo,
			  'descricao' => str_replace("<br />", "\r\n", $reclamacao->descricao),

			  'telefone' => $reclamacao->telefone,
			  'email' => $reclamacao->email,
			  'urlFacebook' => $reclamacao->urlFacebook,
			  'urlTwitter' => $reclamacao->urlTwitter,
			  'urlInstagram' => $reclamacao->urlInstagram,
			  'urlWhatsapp' => $reclamacao->urlWhatsapp,

			  'tipo'      =>$reclamacao->ilustracao_tipo,
			  'estado_conquista' =>$reclamacao->estado_conquista,
			  'imagem'    =>$img,
			  'imagem_depois' =>$img_depois,
			  'imagem_principal' =>$img_principal,
			  //'votos'     =>$votosCount,
			  'categoria' =>$reclamacao->categoria,
			  //'podevotar' =>$podeVotar,
			  'is_admin'  =>$is_admin,
			  'videoId'	  =>$reclamacao->ilustracao_url,
			  'info_arquivo' => $info_arquivo,
			  'info_arquivo_depois' => $info_arquivo_depois
			  );
}

echo json_encode($matriz);
