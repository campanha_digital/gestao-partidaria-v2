<?php

require 'user.php';
if (!$Usuario->is_admin) die();

set_time_limit(90);

$modelsPath = 'models';
$title = 'Modelos gerados com sucesso!';
$msg = '';

try {
    Doctrine::generateModelsFromDb(realpath($modelsPath));
} catch (Doctrine_Import_Builder_Exception $e) {
    $title = 'Falha ao gerar modelos!';
    $msg = $e->getMessage();
}

print $title . "n" . $msg;
