<?php
date_default_timezone_set('America/Sao_Paulo');


/* bootstrap */
// if(session_id() == '') {
	// session_start();
// }

$__PHP_MEMMORY_START = memory_get_usage(); 

require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once 'libraries/Facebook/src/facebook.php';
require_once $appName.'/config/Database.php';
require_once $appName.'/config/App.php';

define("APP_GABINETE_INTERATIVO", 1);
define("APP_TRANSPARENCIA_EFICIENCIA", 2);
define("APP_MODE_ZERO", 0);

 // echo "<pre>" ; print_r( $_SESSION ) ; echo "</pre>" ;
 // $d = json_encode( $_SESSION ) ;
 // echo "<script>alert( $d );</script>";

// if(isset($_SESSION['appNameSpace']) && $_SESSION['appNameSpace'] != $appNamespace && !isset($FROM_LOGOUT)){
	// require_once 'logout.php';
// }
	



/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');



/* FACEBOOK ***************************************************************/
$_USER_FACE_LOGIN = false;


if(isset($_GET['counter']))
	$counter = $_GET['counter']+1;
else 
	$counter = 0;


if(isset($_SESSION['fb_id']) && $_SESSION['fb_id'] != '') {
	$user = $_SESSION['fb_id'];
	$facebook = unserialize($_SESSION['fb_object']);
	// $_FACE_LOGOUT_URL = $facebook->getLogoutUrl();
	// $_FACE_LOGIN_URL = $facebook->getLogoutUrl();
	$_USER_FACE_LOGIN = true;
	
	$token = $_SESSION['fb_token'] ;
	$facebook = new Facebook($facebookAppConfig);
	$facebook->setAccessToken( $token ) ;
	$user = $facebook->getUser();
	$user_profile = $facebook->api('/me');
	$_FACE_LOGOUT_URL = $facebook->getLogoutUrl();
	$_FACE_LOGIN_URL = $facebook->getLogoutUrl();
	$_SESSION['fb_id'] = $user;
	$_SESSION['fb_object'] = serialize($facebook);
	$_SESSION['fb_user_profile'] = $user_profile;
	$_SESSION['appNameSpace'] = $appNamespace;
	
	
} else {
		
	// while($counter < 3) {
		// Create our Application instance (replace this with your appId and secret).
		//$facebookAppConfig vem do instancia/config/App.php
		$facebook = new Facebook($facebookAppConfig);
		
		// Esta variavel $user eh utilizada em varios outros scripts que incluem boostrap.php
		// para verificar se o user ta logado no face. Aos poucos iremos substituir estas
		// verificações por $_USER_FACE_LOGIN
		$user = $facebook->getUser();
		if ($user) {
		  try {
		    // Proceed knowing you have a logged in user who's authenticated.
		    $user_profile = $facebook->api('/me');
		    
		  } catch (FacebookApiException $e) {
				  print_r($e->getMessage());
				  die();
		      error_log($e->getMessage());
		      $user = null;
		      require_once('logout.php');
		      exit;

		  }

		  
		  //algum problema com face , nao temos dados, redirecionamos para tela de login novamente
		  if(empty($user_profile) || !is_array($user_profile))
		  	require_once('logout.php');
		  
		  $_FACE_LOGOUT_URL = $facebook->getLogoutUrl();
		  $_FACE_LOGIN_URL = $facebook->getLogoutUrl();
		  $_USER_FACE_LOGIN = true;
		  $_SESSION['fb_id'] = $user;
		  $_SESSION['fb_object'] = serialize($facebook);
		  $_SESSION['fb_user_profile'] = $user_profile;
		  $_SESSION['appNameSpace'] = $appNamespace;
		//   break;
		//   if(isset($_GET['pagetab']) && !isset($_SESSION['logged']))
		} elseif(isset($_GET['pagetab'])) {
		  $appPermissions['redirect_uri'] = $facebookBaseUrl;
		  $_FACE_LOGIN_URL = $facebook->getLoginUrl($appPermissions);
		} else {
			$appPermissions['redirect_uri'] = $facebookAppConfig['redirect_uri'];
			$_FACE_LOGIN_URL = $facebook->getLoginUrl($appPermissions);
		}
		
		$_USER_FACE_LOGIN = false;
	// 	$counter++;
	// 	sleep(1);
	// }
}

// echo "<pre>" ; print_r( $facebook ) ; echo "</pre>" ;
// echo "<pre>" ; print_r( $_SESSION ) ; echo "</pre>" ;

if(isset($_GET['pagetab']) && !$_USER_FACE_LOGIN) {
	/*//header('Location: '.$facebookBaseUrl);
	?>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
	<?php if(isset($metaOGArray) && is_array($metaOGArray)){
		$img_share_src = null;
		foreach($metaOGArray as $meta_key => $meta_value ){
			echo '<meta property="'.$meta_key.'" content="'.$meta_value.'"/>';
	
			if($meta_key == "og:image")
				$img_share_src = $meta_value;
		}
	
		if($img_share_src)
			echo '<link rel="image_src" href="'.$img_share_src.'" />';
	} ?>
	</head>
<?php */
}


