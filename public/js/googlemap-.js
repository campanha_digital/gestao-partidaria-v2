var geocoder;
var map;
var redmarker;
var globalVotos;
var globalPodeVotar;
var ib2;

globalVotos = new Array();
globalPodeVotar = new Array();

function geo_success(position) {
  var point = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
  redmarker.setPosition(point);  
  map.setZoom(15);
  map.setCenter(point);
  geocoder.geocode({ 'latLng': redmarker.getPosition() }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {                   
                    $('#endereco').removeClass("wrong").val(results[0].formatted_address);
                    if(!$("*").hasClass("wrong"))
                        $(".alertas").slideUp("slow");
                    $('#lat').val(redmarker.getPosition().lat());
                    $('#lng').val(redmarker.getPosition().lng());
            }
        }
    });
}

function geo_error(err) {
}
	
function initialize() {
	
	var boxText = document.createElement("div");
	var boxText2 = document.createElement("div"); 
        boxText2.innerHTML = "<div class='inforapida'><p>Você pode arrastar e soltar este cursor no endereço desejado.</p></div>";
        boxText2.style.cssText = "text-align: center;" ; 
	
        boxText2.getElementsByTagName("p").item(0).style.cssText = "color: #f00;";  
        var myOptions = {
                 content: boxText
                ,disableAutoPan: false
                ,maxWidth: 0
                ,pixelOffset: new google.maps.Size(-185, -240)
                ,zIndex: null
                ,boxStyle: { 
                 width: "356px"
         ,height: "auto"
         ,padding: "10px"
                 }
                ,closeBoxMargin: "-30px !important"
                ,closeBoxURL: '../imgs/x_close_window.png'
                ,infoBoxClearance: new google.maps.Size(1, 1)
                ,isHidden: false
                ,pane: "floatPane"
                ,enableEventPropagation: true               
        };
        
        var myOptions2 = {
                 content: boxText2
                ,disableAutoPan: false
                ,maxWidth: 0
                ,pixelOffset: new google.maps.Size(-112, -110)
                ,zIndex: null
                ,boxStyle: { 
                  background: "url('imgs/bg_verm.png') no-repeat"
                  ,opacity: 1
                  ,width: "207px"
          ,height: "62px"
          ,padding: "12px 10px 0"
                 }
                ,closeBoxMargin: "0"
                ,closeBoxURL: ''
                ,infoBoxClearance: new google.maps.Size(1, 1)
                ,isHidden: false
                ,pane: "floatPane"
                ,enableEventPropagation: true               
        };
                
	//load markers from users update
	$.get("load.php", {qtde: $('#qtde').val(), 
                       code: $('#code').val(),
                       categoria_filtro: $('#categoria_filtro').val(),
                       data_inicio: $('#data_inicio').val(),
                       data_fim: $('#data_fim').val()
                      }, 
        function(data){		
		var marker = [];
		var ib;
		var id = [];
		var isVisible;
		var idlocal = -1;
		
		$.each(data, function(n, reclamacao) {		
			//id.push(n);
			  var icon_reclamacao = "imgs/"+reclamacao['categoria']+".png";
			  //var icon_reclamacao = '';
			  
			  //MARIO: isto estava comentado, fazia com que a marcação do pin que subiu nao aparecesse
			  // descomentei, caso descubra-se porque foi comentado comentar novamente, isto faz o pin
			  // ser ignorado e tem outro comentario mais abaixo que fazia o pin virar a imagem do perfil do cara
			  // dai comentei embaixo e descomentei em cima;
			  if(reclamacao['imagem_principal'] != undefined && reclamacao['imagem_principal'] != ''){
			  	icon_reclamacao = reclamacao['imagem_principal'];
			  } else {
				icon_reclamacao = "imgs/"+reclamacao['categoria']+".png";
			  }

			  marker[n] = new google.maps.Marker({
				position: new google.maps.LatLng(reclamacao['latitude'], reclamacao['longitude']),
				map: map,
				icon: icon_reclamacao,				
				id: n,	
				title: reclamacao['titulo']
			  });
			  
			  if (typeof globalVotos[reclamacao['id']] == 'undefined') globalVotos[reclamacao['id']] = reclamacao['votos'];
			  if (typeof globalPodeVotar[reclamacao['id']] == 'undefined') globalPodeVotar[reclamacao['id']] = reclamacao['podevotar'];
			  
			  ib = new InfoBox(myOptions);
			  
			  var imgPlay = "";
			  if ( reclamacao['tipo']=='video' ) 
			       imgPlay = "<img src='imgs/video-play.png' width='100%' style='position:absolute;margin-top:4px;opacity:0.85;width:125px'>";
			  				  
			
			
			google.maps.event.addListener(marker[n], 'click', (function() {	
			
			
			var voteButton = "";
			var editButton = "";	
			var maisUmVoto = reclamacao['votos'] + 1;
			if (globalPodeVotar[reclamacao['id']] == '1') {
				voteButton = "<span class='button-verde' id='votar"+reclamacao['id']+"' style='margin-right:5px' onclick='javascript:"+
					"globalVotos["+reclamacao['id']+"] = "+maisUmVoto+";"+
					"globalPodeVotar["+reclamacao['id']+"] = \"0\";"+
					"killButton(\"votar"+reclamacao['id']+"\");"+
					"$(\"#votos"+reclamacao['id']+"\").html(\""+maisUmVoto+"\");"+
					"$.getJSON(\"vote.php\","+
					"{ \"usuario_id\":\""+userFacebookId+"\", \"reclamacao_id\":\""+reclamacao['id']+"\" },"+
					"function(data){ "+
					"});'>Curtir</span>";
        	} else {
        		voteButton = "<span class='button-verde' id='votar"+reclamacao['id']+"' title='Você já votou nesta reclamação' style='margin-right:5px;background:#ccc;color:#eee' onclick=\"loginFB(false);\" >Curtir</span>";
        	}

		//MARIO: nao sei porque trocavam este fbpicture para imagem do pin, mas ten de manter a imagem do perfil do cara que criou
		// a marcacao, entao se descobrirem o motivo deste if soh descomentar
		//if(reclamacao['imagem_principal']){
			//reclamacao['fbpicture'] = reclamacao['imagem_principal'];
		//}
		
        if(reclamacao['is_admin']){
            editButton = "<span class='button-verde button-editar' id='editar"+reclamacao['id']+"'  onclick='editar("+reclamacao['id']+")' style='margin-right:5px;'>Editar</span>";
        }
			
		if(reclamacao['imagem_depois']){
			var imagem_antes = "<img src='"+reclamacao['imagem']+"' height='254' alt='Antes' title='Antes' style='height:58px'>";
			var imagem_depois = "<img src='"+reclamacao['imagem_depois']+"' width='381' height='254' style=\"margin-left:10px\" alt='Depois' title='Depois'>";
			var mais_dados_depois = '<span class="label"><br><br><br><a id=\'amplia'+reclamacao['id']+'\' href=\'javascript:void(0)\' onclick=\'javascript:load_modal("reclamacoes-container","amplia","place.php?ajax=1&user='+userFacebookId+'&id='+reclamacao['id']+'");\' class=\'mais-depois\' style=\'background: #fff;padding: 2px 0 0 5px;\'><img src='imgs/bola_red.png' /><span style=\'margin-left:-37px; width:90px; padding-right:20px;\'>ver +</span></a></span>";
			var mais_dados = '';
			var thumb_depois = 'style="height:auto"';
			var tamanhoTextoDepois = 'style="width:290px"';
		} else {
			var imagem_antes = "<img src='"+reclamacao['imagem']+"' width='381' height='254' alt='Antes' title='Antes'  style='width:125px'>";
			var imagem_depois = "";
			var mais_dados = "<span ><a id='amplia"+reclamacao['id']+"' href='javascript:void(0)' onclick='javascript:load_modal(\"reclamacoes-container\",\"amplia\",\"place.php?ajax=1&user="+userFacebookId+"&id="+reclamacao['id']+"\");' class=\'mais-depois\' style=\'background: #fff;padding: 2px 0 0 5px;\'>+ mais</a></span>";
			var mais_dados_depois = '';
			var thumb_depois = '';
			var tamanhoTextoDepois = '';
		}
						  						
					boxText.innerHTML = "<div class='picture'>"+
            	"<a target=\"_blank\" href=\"http://www.facebook.com/"+reclamacao['fb_id']+"\"><img src='"+reclamacao['fbpicture']+"' width='50' height='50'></a>"+
                "<div class='votos'>"+
                    "<span id='votos"+reclamacao['id']+"' class='num-votos'>"+globalVotos[reclamacao['id']]+"</span>"+
                    "<span class='label'>votos</span>"+
		    mais_dados_depois+
                "</div>"+
            "</div>"+
            "<div class='resumo'>"+
            	"<h2  style=\"padding-bottom:15px;padding-top:0px;\">"+reclamacao['titulo']+"</h2>"+
                "<div class='conteudo'>"+
                    "<div class='thumb' "+thumb_depois+">"+
                    imgPlay+
                    imagem_antes+
                    imagem_depois+
                    "</div>"+
                    "<div class='teaser' "+tamanhoTextoDepois+">"+
                        "<p class='depoimento'>"+reclamacao['descricao']+"</p>"+
			mais_dados+
                    "</div>"+
                "</div>"+
                "<div class='buttons' id='popup'>"+
                
                	editButton+ 
                    voteButton+										
                    												
                    "<span class='button-verde' id='share"+reclamacao['id']+"' onclick='javascript:load_modal(\"reclamacoes-container\",\"amplia\",\"place.php?sharenow=1&ajax=1&user="+userFacebookId+"&id="+reclamacao['id']+"\");' >Compartilhar</span>"+
               "</div>"+
            "</div>";
            		
			var isOpen = ib.getMap();
					
					if (isOpen == null){												
						ib.open(map, this);						
						//alert("isOpen: "+isOpen + " --- idlocal: " + idlocal + " thisID: " + this.id + " --- 1 if");
						idlocal = this.id;
						//alert(idlocal);
					} else if((isOpen != null) && (this.id != idlocal)){
						ib.close(map, marker[idlocal]);
						ib.open(map, this);						
						//alert("isOpen: "+isOpen + " --- idlocal: " + idlocal + " thisID: " + this.id + " --- 2 if");
						idlocal = this.id;
						//alert(idlocal);
					} else {
						ib.close(map, this);
						//alert("isOpen: "+isOpen + " --- idlocal: " + idlocal + " thisID: " + this.id + " --- 3 if");							
					}
					
					//ib.open(map, marker[n]);
			  }));
		  });		  		 
		
	},('json'));
    	
    var minZoomLevel = minimumZoom;
    var latlng = new google.maps.LatLng(initialLatitude, initialLongitude);

if( mapCenterConfigLong != null && mapCenterConfigLat != null){
    var centerLatlng = new google.maps.LatLng(mapCenterConfigLat, mapCenterConfigLong);
} else {
    var centerLatlng = new google.maps.LatLng(initialLatitude, initialLongitude);
}
    var options = {
        zoom: initialZoom,
        center: centerLatlng,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    /*var options = {
        zoom: initialZoom,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };*/
 
    map = new google.maps.Map(document.getElementById("map_canvas"), options);
 
    geocoder = new google.maps.Geocoder();	      
	
   //Bounds for North America
   var allowedBounds = new google.maps.LatLngBounds(
     new google.maps.LatLng(boundaryLat1, boundaryLng1), 
     new google.maps.LatLng(boundaryLat2, boundaryLng2));

   //Listen for the dragend event
   google.maps.event.addListener(map, 'dragend', function() {
     if (allowedBounds.contains(map.getCenter())) return;

     // Out of bounds - Move the map back within the bounds

     var c = map.getCenter(),
         x = c.lng(),
         y = c.lat(),
         maxX = allowedBounds.getNorthEast().lng(),
         maxY = allowedBounds.getNorthEast().lat(),
         minX = allowedBounds.getSouthWest().lng(),
         minY = allowedBounds.getSouthWest().lat();

     if (x < minX) x = minX;
     if (x > maxX) x = maxX;
     if (y < minY) y = minY;
     if (y > maxY) y = maxY;

     map.setCenter(new google.maps.LatLng(y, x));
   });      

   // Limit the zoom level
   google.maps.event.addListener(map, 'zoom_changed', function() {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
   });   

   //create redmarker
   //console.log($("#showmarker").attr("data-value"));
   if ( $("#showmarker").attr("data-value")=='true' ) {
    if(navigator && navigator.geolocation) {
        redmarker = new google.maps.Marker({
         /* position: map.getCenter(),*/
            position: latlng,
            map: map,
            draggable: true,        
        icon: "imgs/redmarker.png"
        });
      navigator.geolocation.getCurrentPosition(geo_success, geo_error);
    } else {
	   redmarker = new google.maps.Marker({
         /* position: map.getCenter(),*/
            position: latlng,
	        map: map,
	        draggable: true,		
		icon: "imgs/redmarker.png"
	    });
    }
   } else {
	   redmarker = new google.maps.Marker({
         /* position: map.getCenter(),*/
            position: latlng,
	        map: map,
	        draggable: false,
	        visible: false,		
		icon: "imgs/redmarker.png"
	    });
   }
   
   google.maps.event.addListener(redmarker, "mouseover", function(event) {    			
        ib2 = new InfoBox(myOptions2);
		ib2.open(map, this);	
   });
   
   google.maps.event.addListener(redmarker, "mouseout", function(event) {    						
		ib2.close(map, this);	
   });
   
   google.maps.event.addListener(redmarker, "dragend", function(event) {	
   //alert(this.getPosition());	
        geocoder.geocode({ 'latLng': redmarker.getPosition() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {  					
                    $('#endereco').removeClass("wrong").val(results[0].formatted_address);
					if(!$("*").hasClass("wrong"))
						$(".alertas").slideUp("slow");
                    $('#lat').val(redmarker.getPosition().lat());
                    $('#lng').val(redmarker.getPosition().lng());
                }
            }
        });
		
		var c = redmarker.getPosition(),
         x = c.lng(),
         y = c.lat(),
         maxX = allowedBounds.getNorthEast().lng(),
         maxY = allowedBounds.getNorthEast().lat(),
         minX = allowedBounds.getSouthWest().lng(),
         minY = allowedBounds.getSouthWest().lat();

     if (x < minX) x = minX;
     if (x > maxX) x = maxX;
     if (y < minY) y = minY;
     if (y > maxY) y = maxY;

     redmarker.setPosition(new google.maps.LatLng(y, x));	
		
    });	            		         
   
}
 
function killButton(buttonId){
	$('#'+buttonId).click( function(){return false;} );
	$('#'+buttonId).attr( 'onclick' , 'return false;' );
	$('#'+buttonId).css('background','#ccc');
	$('#'+buttonId).css('color','#eee');
}
 
$(window).load(function () {	
	initialize();		

    $( "#dialog-message-ok" ).dialog({
            autoOpen: false,
            minWidth: 300,
            minHeight: 200,
            resizable: false,
            buttons: {
                Ok: function() {
                $( this ).dialog( "close" );
                }
            }
    });    
	
	function carregarNoMapa(endereco) {
        geocoder.geocode({ 'address': endereco + ', ' + window.cityName, 'region': window.regionCode }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if($("#sel_autocomplete").val() == 1 || $('#cobertura').val() == 1){
                    if (results[0]) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
     
                        $('#endereco').val(results[0].formatted_address);
                        $('#lat').val(latitude);
                        $('#lng').val(longitude);
     
                        var location = new google.maps.LatLng(latitude, longitude);
                        redmarker.setPosition(location);
                        map.setCenter(location);
                        map.setZoom(16);
                        $("#sel_autocomplete").val('0');
                    }
                } else {
                    $('#lat').val(redmarker.getPosition().lat());
                    $('#lng').val(redmarker.getPosition().lng());
 
                    map.setCenter(redmarker.getPosition());
                    map.setZoom(16);

                    $("#dialog-message-ok" ).dialog('open');

                    $('.endereco_msg').html($('#endereco').val());
                    $("#sel_autocomplete").val('0');
                }
            }
        });
    }
	
	$("#endereco").blur(function() {  	   
        var valor = $(this).val();
		if(valor != ""){
            setTimeout(function(){
                if(map.getBounds().contains(redmarker.getPosition())){
    				carregarNoMapa(valor);				
    				if($(".alertas").is(":visible"))
    					$(".alertas").slideUp("slow");
    			}else{				
    				$('.alertas').html("Só são válidos endereços dentro da cidade de " + window.cityName).slideDown("slow");
    			}
            },'500');
		}
    })
	
	$("#endereco").autocomplete({
        source: function (request, response) {
            geocoder.geocode({ 'address': request.term + ', ' + window.cityName + ', ' + window.regionCode, 'region': window.regionCode }, function (results, status) {
                response($.map(results, function (item) {
                    return {
                        label: item.formatted_address,
                        value: item.formatted_address,
                        latitude: item.geometry.location.lat(),
                        longitude: item.geometry.location.lng()
                    }
                }));
            })
        },
        select: function (event, ui) {
            $("#sel_autocomplete").val('1');
            $("#lat").val(ui.item.latitude);
            $("#lng").val(ui.item.longitude);
            var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
            redmarker.setPosition(location);
            map.setCenter(location);
            map.setZoom(16);			
        }
		
    });		

    var streetViewService = new google.maps.StreetViewService();
    var STREETVIEW_MAX_DISTANCE = 100;
    //var latLng = new google.maps.LatLng(-19.98411, -44.85733);
    var latLng = map.getCenter();
    streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
        if (status === google.maps.StreetViewStatus.OK) {
            $('#cobertura').val('1');
	    $('#dialog-message').hide();
        } else {
            $('#cobertura').val('0');
            $( "#dialog-message" ).dialog({
            modal: true,
                buttons: {
                    Ok: function() {
                    $( this ).dialog( "close" );
                    }
                },
                minWidth: 350,
                minHeight: 200,
                resizable: false
            });
        }
    });
});

function editar(idParam) {
    $('#editar'+idParam).html('Carregando');   
    $.post( "edit.php", { id: idParam},function( data ) {
        
        $('#reclamacaoId').val(data.id);
        $('#endereco').val(data.endereco);
        $('#titulo').val(data.titulo);
        $('#lat').val(data.latitude);
        $('#lng').val(data.longitude);
        $("#choice").selectbox('detach');
        $("#choice").val(data.categoria);
        $("#choice").selectbox('attach');
        
        $("#estado_conquista").selectbox('detach');
        $("#estado_conquista").val(data.estado_conquista);
        $("#estado_conquista").selectbox('attach');
        
        $('#descricao').val(data.descricao);
        $("input[name='youtube_url']").val(data.videoId);

        if(data.tipo == 'video') {

            $("div.file").hide();
            $("div.file-depois").hide();
            $("div.video").show();
            $("#tipoUpload").val('2');
            $('.second').addClass('active');
            $('.first').removeClass('active');
            $("input[name='youtube_url']").val(data.videoId);
            $('.info-arquivo').html('<b>Pre-Conquista</b>');
            $('.info-arquivo-depois').html('<b>Pós-Conquista</b>');

        } else {

            $("div.video").hide();
            $("div.file").show();
            $("div.file-depois").show();
            $("#tipoUpload").val('1');
            $('.first').addClass('active');
            $('.second').removeClass('active');
            $("input[name='youtube_url']").val('');

            if(data.info_arquivo)
                $('.info-arquivo').html(data.info_arquivo);
            if(data.info_arquivo_depois)
                $('.info-arquivo-depois').html(data.info_arquivo_depois);
        }

        $('#editar'+idParam).html('Editar');
        $('.titulo').html('<span class="minus-plus"></span>Editar Dados');
        
        $(".formulario").slideDown("slow", function(){
            $(".menu h2").css("background-position", "0 0");
        }); 
        
    }, "json");
}
