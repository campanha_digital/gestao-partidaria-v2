$(document).ready(function(e) {	
	
	$(".formulario").slideUp("slow", function(){
        $(".menu h2").css("background-position", "-223px 0");
      });      
	  $(".formulario_filtro").slideUp("slow", function(){
          $(".menu h2").css("background-position", "-223px 0");
      });
      $(".social-phone").slideUp("slow", function(){
          $(".menu h2").css("background-position", "-223px 0");
      });
      
      
      
      
      window.mobilecheck = function() {
          var check = false;
          (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
          return check; 
        }

        if(mobilecheck() == true){
            $(".instrucao").css('display','none');
            $(".recuar-txt").html("Informações");
//            $('.btn-recuar').addClass("flip-vertical");    
            $(".recuar").addClass("move-down");
        }
      
      
      
//limit caracteres 
	$("#container, #map_canvas").height($(document).height() + 100);
       /* $(".btn-recuar").mouseover(function(){
			$(".recuar-txt").show("fast");	
		});
		
		$(".btn-recuar").mouseout(function(){
			$(".recuar-txt").hide("fast");	
		});*/
		
		$(".recuar, .recuar-txt, .btn-recuar").click(function(){
			if ($('.instrucao').is(':animated')) return false;
			if($(".instrucao").is(":visible")){								
				$(".instrucao").slideUp("slow");
				$(".recuar").addClass("move-down");
//				$(this).addClass("flip-vertical");
				$(".recuar-txt").html("expandir");
			}else{				
				$(".instrucao").slideDown("slow", function(){
					$(".btn-recuar").removeClass("flip-vertical");
					$(".recuar").removeClass("move-down");
				});
				$(".recuar-txt").html("recolher");
				
			}
		});

		$(".recuar-txt").click(function(){
			if ($('.instrucao').is(':animated')) return false;
			if($(".instrucao").is(":visible")){								
				$(".instrucao").slideUp("slow");
				$(".recuar").addClass("move-down");
				/*$(this).addClass("flip-vertical");*/
				$(".recuar-txt").html("expandir");
			}else{				
				$(".instrucao").slideDown("slow", function(){
//					$(".btn-recuar").removeClass("flip-vertical");
					$(".recuar").removeClass("move-down");
				});
				$(".recuar-txt").html("recolher");
				
			}
		});
		
		$(".button").click(function () {
      var upl = $(this).attr('rel');
			$("#"+upl).trigger('click');
		});

    $(".button-depois").click(function () {
      var upl = $(this).attr('rel');
      $("#"+upl).trigger('click');
    });

    $(".button-principal").click(function () {
      var upl = $(this).attr('rel');
      $("#"+upl).trigger('click');
    });
    		
		$('.tabnav li').click( function() {
		  	$(this).addClass('active').siblings().removeClass('active');
			if($(this).hasClass("second")) { 
				$("div.file").hide();
        $("div.file-depois").hide();
				$("div.video").show();
				$("#tipoUpload").val('2');
			}else if($(this).hasClass("first")) {
				$("div.video").hide();
				$("div.file").show();
        $("div.file-depois").show();
				$("#tipoUpload").val('1');
			}
		});						
		
		var clearMePrevious = "";

		// clear input on focus
		$('input[type=text], textarea').focus(function()
		{
			if($(this).hasClass("wrong"))
				$(this).removeClass("wrong");
				
			if($(this).val()==$(this).attr("original"))
			{
				clearMePrevious = $(this).val();
				$(this).val('');
			}
		});
		
		//$("#choice").selectbox();
		$("#choice").selectbox({
			onChange: function (val, inst) {												
				if($(".sbHolder").hasClass("wrong"))
					$(".sbHolder").removeClass("wrong");
			},
			
		});


    $("#estado_conquista").selectbox({
	classOptions: 'sbOptionsEstado',
        onChange: function (val, inst) { 
			if(val == "1"){
				$('#sbSelector_'+inst.uid).css('background', '#8DC63F');
			}else if(val == "2"){
				$('#sbSelector_'+inst.uid).css('background', '#FEF202');
			}else if(val == "3"){
				$('#sbSelector_'+inst.uid).css('background', '#F16522');
			}
			
            if($(".sbHolder").hasClass("wrong"))
                $(".sbHolder").removeClass("wrong");
        },
        
    });
		
		$("#categoria_filtro").selectbox({
      onChange: function (val, inst) {                        
        if($(".sbHolder").hasClass("wrong"))
          $(".sbHolder").removeClass("wrong");
      },
      
    });

    $("#data_inicio").datepicker({
      dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior'
    });


    $("#data_fim").datepicker({
      dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior'
    });

    $("#data_realizacao").datepicker({
      dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior'
    });

    $(document).click(function(e) { 
      var ele = $(e.target); 
      if (!ele.hasClass("hasDatepicker") && 
          !ele.hasClass("ui-datepicker") && 
          !ele.hasClass("ui-icon") && 
          !ele.hasClass("ui-datepicker-next") && 
          !ele.hasClass("ui-datepicker-prev") && 
          !$(ele).parent().parents(".ui-datepicker").length)
         $(".hasDatepicker").datepicker("hide"); 
    });
		
		// if field is empty afterward, add text again
		$('input[type=text], textarea').blur(function()
		{			
			if(($(this).val()=='') && (clearMePrevious == $(this).attr("original")))
			{
				$(this).val(clearMePrevious);
			}else if(($(this).val()=='') && (clearMePrevious != $(this).attr("original"))){
				$(this).val($(this).attr("original"));
			}
		});					
		
		$( "div.colEsquerda" ).draggable({ handle: "h2", containment: "#container", scroll:false });
		$( "div.participe" ).draggable({ handle: "h2", containment: "#container", scroll:false });
		
		$("#participe-mobile").click(function(){      
	      if($(".formulario").is(":visible")) {
	        $(".recuar").css("z-index","801");
	        $(".formulario").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	        });       
	      }else{
	        $(".recuar").css("z-index","700");
	        $(".formulario").slideDown("slow", function(){
	          $(".menu h2").css("background-position", "0 0");
	        }); 
	      }
	      $(".formulario_filtro").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	      });
	      $(".social-phone").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	      });
	    });

	    $("#filtros-mobile").click(function(){      
	      if($(".formulario_filtro").is(":visible")) {
	        $(".recuar").css("z-index","801");
	        $(".formulario_filtro").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	        });       
	      }else{
	        $(".recuar").css("z-index","700");
	        $(".formulario_filtro").slideDown("slow", function(){
	          $(".menu h2").css("background-position", "0 0");
	        }); 
	      }
	      $(".formulario").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	      });
	      $(".social-phone").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	      });
	    }); 

	    $("#compartilhe-mobile").click(function(){      
	      if($(".social-phone").is(":visible")) {
	        $(".recuar").css("z-index","801");
	        $(".social-phone").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	        });       
	      }else{
	        $(".recuar").css("z-index","700");
	        $(".social-phone").slideDown("slow", function(){
	          $(".menu h2").css("background-position", "0 0");
	        }); 
	      }
	      $(".formulario").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	      });
	      $(".formulario_filtro").slideUp("slow", function(){
	          $(".menu h2").css("background-position", "-223px 0");
	      });
	    }); 
		
		
	    
	    //MINUS PLUS DO MARIO
		$(".minus-plus").click(function(){			
				if($(".formulario").is(":visible")) {
					$(".formulario").slideUp("slow", function(){
						$(".menu h2[id=colEsquerda]").css("background-position", "-223px 0");
					});				
				}else{
					$(".formulario").slideDown("slow", function(){
						$(".menu h2[id=colEsquerda]").css("background-position", "0 0");
					});	
				}
			});	

	    $(".minus-plus-filtro").click(function(){      
	      if($(".formulario_filtro").is(":visible")) {
	        $(".formulario_filtro").slideUp("slow", function(){
	          $(".menu h2[id=colDireita]").css("background-position", "-223px 0");
	        });       
	      }else{
	        $(".formulario_filtro").slideDown("slow", function(){
	          $(".menu h2[id=colDireita]").css("background-position", "0 0");
	        }); 
	      }
	    });
		
			
		
		
		// MODAL
		
		//get the height and width of the page
    var window_width = $("#container").width();
    var window_height = $("#container").height();

    //vertical and horizontal centering of modal window(s)
    /*we will use each function so if we have more then 1
    modal window we center them all*/
    $('.modal-window').each(function(){
        //get the height and width of the modal
        var modal_height = $(this).outerHeight();
        var modal_width = $(this).outerWidth();

        //calculate top and left offset needed for centering
        var top = $(window).height()/2 - $(this).height()/2;
        var left = $("#mask").width()/2  - $(this).width()/2;       

        //apply new top and left css values
        $(this).css({'top' : top , 'left' : left});
        
    });		

    $('#ranking').click(function(e){
	load_modal('ranking-container','ranking','ranking.php');
    });
    $('.close-modal').click(function(){
	//player = new YT.Player('player');
//	player.stopVideo();
	$("#player").attr("src",'');
        //use the function to close it
        close_modal();
    });

	$(".tripa").scrollbars();
	$("#comentarios").scrollbars();
});	  	  	  	 
//THE FUNCTIONS

function load_modal(modal_id,modal_class,modal_script) {
    //set display to block and opacity to 0 so we can use fadeTo
    $('#mask').css({ 'display' : 'block', opacity : 0.7});

    //fade in the mask to opacity 0.8
    $('#mask').fadeTo(500,0.8);
    
        $('#'+modal_id).load(modal_script,function(){
        	show_modal(modal_class);
        });
       
}

function load_share(url) {

	$.post( url, function( data ) {
	 
		postOnFacebook( data.message, data.name, data.caption, data.description, data.link, data.picture, data.user_message );
		
    }, "json");
       
}

function close_modal(){
    //hide the mask
    $('#mask').fadeOut(500);

    //hide modal window(s)
    $('.modal-window').fadeOut(500);
    
    $('#closeimg').remove();

}
function show_modal(modal_id){
    //show the modal window
    $('.' + modal_id).fadeIn(500);
    $('.' + modal_id).append("<img id='closeimg' src='../imgs/x_close_window.png' style='position:absolute;right:-20px;top:-20px;z-index:1;cursor:pointer;z-index:10000' onclick='$(\"#mask\").click()' />");
}	  
	  
/* function to display file name when selected */
$.fn.fileName = function() {	
	var $this = $(this),
	$val = $this.val(),
	valArray = $val.split('\\'),
	newVal = valArray[valArray.length-1],
	$button = $this.siblings('.info-arquivo');
	if(newVal !== '') {		
		$button.text(newVal);				
		$("#caminhoArquivo").val(newVal);
		$("#drop-files-depois").slideUp("slow");
		if($(".file").hasClass("wrong"))
			$(".file").removeClass("wrong");		
  	}
};

$.fn.fileNameDepois = function() {  
  var $this = $(this),
  $val = $this.val(),
  valArray = $val.split('\\'),
  newVal = valArray[valArray.length-1],
  $button = $this.siblings('.info-arquivo-depois');
  if(newVal !== '') {   
    $button.text(newVal);       
    $("#caminhoArquivoDepois").val(newVal);
    $("#drop-filesDepois").slideUp("slow");
    if($(".file-depois").hasClass("wrong"))
      $(".file-depois").removeClass("wrong");    
    }
};

$.fn.fileNamePrincipal = function() {  
  var $this = $(this),
  $val = $this.val(),
  valArray = $val.split('\\'),
  newVal = valArray[valArray.length-1],
  $button = $this.siblings('.info-arquivo-principal');
  if(newVal !== '') {   
    $button.text(newVal);       
    $("#caminhoArquivoPrincipal").val(newVal);
    $("#drop-filesPrincipal").slideUp("slow");
    if($(".file-principal").hasClass("wrong"))
      $(".file-principal").removeClass("wrong");    
    }
};

$().ready(function() {
	/* on change, focus or click call function fileName */
	$('input[type=file]').bind('change focus click', function() {		
    if($(this).attr('id') == 'fileUpload'){
		  $(this).fileName();		
    }
    else if($(this).attr('id') == 'fileUploadDepois') {
      $(this).fileNameDepois();   
    }
    else if($(this).attr('id') == 'fileUploadPrincipal') {
      $(this).fileNamePrincipal();   
    }
	});	
});

function postOnFacebook( message, name, caption, description, link, picture, user_message ) 
{		
	FB.ui(
	  {
	   method: 'feed',
	   message: message,
	   name: name,
	   caption: caption,
	   description: ( description ),
	   link: link,
	   picture: picture,
	   user_message_prompt: user_message
	  },
	  function( response ) {
		  console.log( response );
	    if ( response && response.post_id ) {
	      //alert('Esta reclamação foi publicada na sua Timeline!');
	    } else {
	      //alert('Esta reclamação não foi Publicada.');
	    }
	  }
	);
	
}
