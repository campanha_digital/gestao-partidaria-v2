$(document).ready(function(e) {
    
    //Drag and drop files
    if ($.browser.msie) {
        $("#drop-files").hide();
        //$("#send").hide();
        //$("#send-ie").show();
	/*$(".alertas").html("Para melhor navegação, usar o os navegadores <a href='https://www.google.com/intl/pt-BR/chrome/browser/?hl=pt-BR&brand=CHMB&utm_campaign=pt-_br&utm_source=pt-br-ha-latam-br-sk&utm_medium=ha' target='_blank'>Chrome</a> ou <a href='http://www.mozilla.org/pt-BR/firefox/fx/' target='_blank'>Firefox</a>.").show();*/
    }
		
    jQuery.event.props.push('dataTransfer');
     
    var z = -40;
    // The number of images to display
    var maxFiles = 3;
    var errMessage = 0;
     
    // Get all of the data URIs and put them in an array
    var dataArray = [];
		
$('#drop-files').bind('drop', function(e) {
    
    // This variable represents the files that have been dragged
    // into the drop area
    var files = e.dataTransfer.files;
     	 	 
    // Show the upload holder    
	$('#uploaded-holder').show();
     
    // For each file
    $.each(files, function(index, file) {
		// Some error messaging
		
		if (!files[index].type.match('image.*')) {
			 
			if(errMessage == 0) {
				$('#drop-files').html('Insira somente imagens');        
			}    
			return false;
		}

		//aqui

		var fileReader = new FileReader();
			 
			// When the filereader loads initiate a function
			fileReader.onload = (function(file) {
				 
				return function(e) {
					 
					// Push the data URI into an array
					dataArray.push({name : file.name, value : this.result});										
					 
					// This is the image
					var image = this.result;
					var nome = file.name;
					// Just some grammatical adjustments
							if(dataArray.length == 1) {						                     															
								$('#dropped-files').append('<div class="image" style="left: '+z+'px; background: url('+image+'); background-size: cover;"> </div>');
								$('#upload-button span').html(nome);								
								$("#caminhoArquivo").val(image);																																
								if($('#dropped-files > .image').length < maxFiles) {
									// Change position of the upload button so it is centered
									var imageWidths = ((220 + (40 * $('#dropped-files > .image').length)) / 2) - 20;
									$('#upload-button').css({'left' : imageWidths+'px', 'display' : 'block'});
									
									if($(".alertas").is(":visible"))
										$(".alertas").slideUp("slow");
									$(".button, .info-arquivo, #drop-files").slideUp('slow', function(){
										if($(".file").hasClass("wrong"))
										$(".file").removeClass("wrong");
									});
									$('#upload-button').css('margin-top', '10px');
									
								}
							} else {
								dataArray.length = 0; 															
								$('.alertas').html("Favor fazer upload de apenas 1 imagem.").show();								
								return false;								
							}																				
						};
						 
					})(files[index]); //each files
								
					fileReader.readAsDataURL(file);
						 		
			}); //dropfiles .bind
			
}); //ready function






//valida e envia o formulario
	
	if ($.browser.msie) {
		
		$("#send").bind('click',function(e){
			if (!validaFormulario(e)) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		});
		
		/*
		$("#send-ie").click(function(e) {
												
			if (!validaFormulario(e)) return false;
			
			$(".alertas").addClass('sucesso');
			$(".alertas").html("Sua opinião foi enviada e será publicada em alguns minutos.");
			$(".alertas").show();
			
			$("#send").trigger('click');
			
		});*/
						
	} else {
	
		$("#uploader").ajaxForm(
		{
			target: "#alertas",
			success: function(data) {
				
				// var result = jQuery.parseJSON( data );
				
				initialize();
				
				//postOnFacebook(result.message, result.name, result.caption, result.description, result.link, result.picture, result.user_message );
				
				$('#loading-bar .loading-color').css({'width' : '100%'});
				$('#loading-content').html('100%');
				
				restartFiles();
					
				$(".alertas").addClass('sucesso').html("Seu cadastro foi enviado e será publicado em alguns minutos.").slideDown("slow").delay(10000).slideUp('slow', function(){
					$(this).removeClass('sucesso');
				});
					
          $('#loadinggif').hide();
          $('#send').show();
			  	},
			error: function(data) {
					$(".alertas").html("Erro ao salvar o arquivo. Tente novamente mais tarde.").slideDown("slow").delay(7000).slideUp('slow');
				},
			uploadProgress: function(event, position, total, percentComplete) {
        
					$('#loading').css({'display' : 'block'});
					
				        var percentVal = (percentComplete-1) + '%';
				        $('#loading-bar .loading-color').css({'width' : percentVal});
				        $('#loading-content').html(percentVal);
			        
				},
			beforeSubmit: function(e){											
					if (!validaFormulario(e)) return false; 
          else {
            $('#send').hide();
            $('#loadinggif').show();
          }
				}
		});
		
	}
		

function validaFormulario(e) {

	var percentVal = '0%';
	var value = $("#endereco").val();
	if(window.cityName.length > 0){
		if (value.indexOf(window.cityName) < 0) {
			$("#endereco").addClass("wrong");
			$(".alertas").html("Seu endereço deve fazer parte de " + window.cityName).slideDown("slow");					
			return false;
		}
	} else if (window.regionCode.length > 0) {
		if (value.indexOf(window.regionCode) < 0) {
			$("#endereco").addClass("wrong");
			$(".alertas").html("Seu endereço deve fazer parte de " + window.regionCode).slideDown("slow");					
			return false;
		}
	}
	
	//checa campos
	$("formulario:input").each(function() {
	   if(($(this).val() === $(this).attr("original")) && $(this).is(":visible")){
			$(this).addClass("wrong");
			$(".video input").removeClass("wrong");	
	   }else{
			$(this).removeClass("wrong");
	   }				   
	});																								
	
	if($("#choice").val() == 0)
		$(".sbHolder").addClass("wrong");											

	if($("#titulo").val() == "Do que se trata sua opinião?")
		$("#titulo").addClass("wrong");											
    
	if($("#whatsapp").val() == "WhatsApp (00)00000-0000")
		$("#whatsapp").addClass("wrong");											
    
	if($("#descricao").val() == "Descreva sua opinião")
		$("#descricao").addClass("wrong");											
    
    
	
	/*
	if($(".file").is(":visible") && $("#caminhoArquivo").val() == ""){
		$(".file").addClass("wrong");
	}else{
		$(".file").removeClass("wrong");					
	}
	
	if($(".video").is(":visible") && $(".video input").val() == $(".video input").attr("original")){
		$(".video input").addClass("wrong");
	}else{
		$(".video input").removeClass("wrong");					
	}
	*/
										
	if($("*").hasClass("wrong")){					
		$(".alertas").html("Preencha os campos em vermelho").slideDown("slow");					
		return false;
	}else{
		if($(".alertas").is(":visible"))
			$(".alertas").hide();
	}

	return true;
	
}
			

function restartFiles() 
{

    $("#uploader").resetForm();   
    $('#uploader').clearForm();
	
    if($(".alertas").is(":visible"))
    $('.alertas').slideUp("slow");
    $('#drop-files').css('margin-top', '0');
    $(".button, .info-arquivo, #drop-files").slideDown('slow');
    $(".button-depois, .info-arquivo-depois, #drop-files-depois").slideDown('slow');
    $('#upload-button').css('margin-top', '0');
    $("#caminhoArquivo").text("");
 
    // This is to set the loading bar back to its default state
    $('#loading-bar .loading-color').css({'width' : '0%'});
    $('#loading').css({'display' : 'none'});
    $('#loading-content').html(' ');
    // --------------------------------------------------------
     
    // We need to remove all the images and li elements as
    // appropriate. We'll also make the upload button disappear
    $('#upload-button').hide();
    $('#dropped-files > .image').remove();    
    $('#uploaded-holder').hide();
    
    //Restaura os valores originais dos boxes
    $(":input").each(function() {
    	if ($(this).attr("original") != "")
	   $(this).val( $(this).attr("original") );									   
    });
    
    
    
    $('#fileUploadPrincipal').val("");   
    $('#caminhoArquivoPrincipal').val("");
    $('.info-arquivo-principal').text("");
    
    $('#fileUpload').val("");
    $('#caminhoArquivo').val(""); 
    $('.info-arquivo').text(""); 
    
    $('#fileUploadDepois').val("");
    $('#caminhoArquivoDepois').val("");
    $('.info-arquivo-depois').text("");
    
    $('.sbSelector').html('Categoria da opinião');
 
    // And finally, empty the array/set z to -40
    dataArray.length = 0;    
     
    return false;
}

$('#upload-button .upload').click(function() {
     
			// Show the loading bar
			$("#loading").show();
			// How much each element will take up on the loading bar
			var totalPercent = 100 / dataArray.length;
			// File number being uploaded
			var x = 0;
			var y = 0;
			 
			// Show the file name
			$('#loading-content').html('Uploading '+dataArray[0].name);
			 
			 
			// Upload each file separately
			$.each(dataArray, function(index, file) { 
			// Post to the upload.php file
		$.post('upload.php', dataArray[index], function(data) {
		 
			// The name of the file
			var fileName = dataArray[index].name;
			++x;
			 
			// Change the loading  bar to represent how much has loaded
			$('#loading-bar .loading-color').css({'width' : totalPercent*(x)+'%'});
			 
			if(totalPercent*(x) == 100) {
				// Show the upload is complete
				$('#loading-content').html('Upload Completo!');
				 
				// Reset everything when the loading is completed
				setTimeout(restartFiles, 200);
				 
			} else if(totalPercent*(x) < 100) {
			 
				// Show that the files are uploading
				$('#loading-content').html('Fazendo Upload de '+fileName);
			 
			}
       
        });
    });
     
    return false;
});

// Just some styling for the drop file container.
$('#drop-files').bind('dragenter', function() {
    $(this).css({'box-shadow' : 'none', 'border' : '2px dashed #AAAAAA', 'width' : '177px'});
    return false;
});
 
$('#drop-files').bind('drop', function() {
    $(this).css({'box-shadow' : 'none', 'border' : 'none'});
    return false;
});

 
// Restart files when the user presses the delete button
$('#dropped-files #upload-button .delete').click(restartFiles);
		
});
