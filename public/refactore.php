<?php
$__START_TIME = time();


/* load */
require_once "user.php";
require_once "functions.php";

$q = Doctrine_Query::create()
        ->from('Reclamacoes');
//@WCS
//quantidade de limite
/*if($_GET['qtde']){
		$q->limit($_GET['qtde']);
}*/

if($_GET['categoria_filtro']) {
	$q->andwhere('categoria = "'.$_GET['categoria_filtro'].'"');
}

if($_GET['data_inicio'] != 'Escolha uma data de início' && $_GET['data_inicio'] != '') {
	$data_inicio = explode('/',$_GET['data_inicio']);
	$_GET['data_inicio'] = $data_inicio['2'].'-'.$data_inicio['1'].'-'.$data_inicio['0'];
//	$q->where('aprovada_em = ?',);	
	$q->andwhere("aprovada_em > '" . $_GET['data_inicio'] . "'");
}

if($_GET['data_fim'] != 'Escolha uma data máxima' && $_GET['data_fim'] != '') {
	$data_fim = explode('/',$_GET['data_fim']);
	$_GET['data_fim'] = $data_fim['2'].'-'.$data_fim['1'].'-'.$data_fim['0'];
	$q->andwhere("aprovada_em < '" 	. $_GET['data_fim'] . "'");
}

$q->orderBy('aprovada_em', 'desc');
$Reclamacoes = $q->execute();

$matriz = array();
foreach ($Reclamacoes as $reclamacao) {
	
	$q = Doctrine_Query::create()
                ->from('Votos')
                ->where('reclamacao_id = ?', $reclamacao->id);

	$Votos = $q->execute();
	$votosCount = count($Votos);
	$podeVotar = '1';
	
	foreach ($Votos as $voto) {
		if ($voto->usuario_id == $user) $podeVotar = '0';
	}
	
	if (!$user) $podeVotar = '0';
	
	$img = $reclamacao->ilustracao_url;
	$img_depois = $reclamacao->ilustracao_url_depois;
	$img_principal = $reclamacao->ilustracao_url_principal;
	
	if ($reclamacao->ilustracao_tipo == 'video') {
	
		$lastpart = strstr($img,'&');
		$videoId = str_replace(array("http://www.youtube.com/watch?v=","https://www.youtube.com/watch?v=",$lastpart),"",$img);		
		
		$img = "https://i.ytimg.com/vi/".$videoId."/hqdefault.jpg";
		
	}
	
	if(is_object($Usuario) && $Usuario->is_admin){
		$is_admin = 1;
	} else {
		$is_admin = null;
	}

	$__PHP_MEMMORY_END = memory_get_usage()/1024/1024;
	$__END_TIME = time();	
	
	$_img = basename( $img );
	$_img_depois = basename( $_img_depois );
	$_img_principal = basename( $_img_principal );
	
	if ( $_img != 'hqdefault.jpg') {
		
		if ( !strrpos($_img, 'GoogleStreetView') ) {
			
			if ( !empty( $_img ) ) {
			
				$oldname = $uploadDir . $_img;
				
				$extension = substr( $_img, -3 );
				
				$_img = str_replace( '_', " ", $_img );
				$_img = str_replace( '.'.$extension, "", $_img );
				$_img = replace_chars( $_img ) . '.' . $extension;
				
				$newname = $uploadDir . $_img;
				
				if ( rename($oldname, $newname) ) {
				
				$reclamacao->ilustracao_url = $headerUrl . 'imgs/uploaded/' . $_img;
				$reclamacao->save();
				
				echo $reclamacao->ilustracao_url;
				}
				
				echo ' = ' . $_img; 
				echo '<br>';
			}
			
			if ( !empty( $_img_depois ) ) {
					
				$oldname = $uploadDir . $_img_depois;
			
				$extension = substr( $_img_depois, -3 );
			
				$_img_depois = str_replace( '_', " ", $_img_depois );
				$_img_depois = str_replace( '.'.$extension, "", $_img_depois );
				$_img_depois = replace_chars( $_img_depois ) . '.' . $extension;
			
				$newname = $uploadDir . $_img_depois;
			
				if ( rename($oldname, $newname) ) {
			
					$reclamacao->ilustracao_url_depois = $headerUrl . 'imgs/uploaded/' . $_img_depois;
					$reclamacao->save();
			
					echo $reclamacao->ilustracao_url_depois;
				}
			
				echo ' = ' . $_img_depois;
				echo '<br>';
			}
			
			if ( !empty( $_img_principal ) ) {
					
				$oldname = $uploadDir . $_img_principal;
					
				$extension = substr( $_img_principal, -3 );
					
				$_img_principal = str_replace( '_', " ", $_img_principal );
				$_img_principal = str_replace( '.'.$extension, "", $_img_principal );
				$_img_principal = replace_chars( $_img_principal ) . '.' . $extension;
					
				$newname = $uploadDir . $_img_principal;
					
				if ( rename($oldname, $newname) ) {
						
					$reclamacao->ilustracao_url_principal = $headerUrl . 'imgs/uploaded/' . $_img_principal;
					$reclamacao->save();
						
					echo $reclamacao->ilustracao_url_principal;
				}
					
				echo ' = ' . $_img_principal;
				echo '<br>';
			}
		}
		
		
	}
	
	
	
// 	$matriz[] = array('id'        => $reclamacao->id,
// 			  'imagem'    => $img,
// 			  'imagem_depois' => $img_depois,
// 			  'imagem_principal' => $img_principal
// 			  );
	
}

var_dump('<pre>',$matriz,'</pre>');

//echo json_encode($matriz);
