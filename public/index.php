<?php
$__TIME_START = time();
session_start();

// ini_set( 'display_errors', 1 ) ;
// ini_set( 'display_startup_errors', 1 ) ;
// error_reporting( E_ALL ) ;
// echo "<pre>" ; print_r( $_SESSION ) ; echo "</pre>" ;

/* index */
// if (empty($appName)) 
// 	header('Location: https://governodigital.net.br');
// if( !isset( $_GET['categoria_filtro'] ) || !isset( $_GET['data_inicio'] ) || !isset( $_GET['data_fim'] ) ) {
	require_once "user.php";	
// }

$__PHP_MEMMORY_LOAD = memory_get_usage();

if (empty($headerImg)) 
	$headerImg = 'header.png';

if (empty($appMode)) 
	$appMode = APP_MODE_ZERO;

$showform = 1;
if(!isset($appTransparencia))
	$appTransparencia = false;

if(!isset($appTitleStyle))
	$appTitleStyle = null;

if(!isset($menuH2Style))
	$menuH2Style = null;

if(!isset($menuH2SetaStyle))
	$menuH2SetaStyle = null;

 if(isset($_GET['pagetab']))
 	$targetHeader = "_blank";
 else 
 	$targetHeader = "_top";
 
//MARIO: este ajuste temporario esta fazendo o saopaulo ficar aberto para todos. Removi e deixei somente consumidor pois 
// nao sei se deve ficar com form ou nao.
if(strstr($_SERVER['PHP_SELF'],'consumidor') == true ){
    $showform = 1;
}

//MARIO: tava comentado isso, acho que tem a ver com o POG acima
if ($appMode == APP_TRANSPARENCIA_EFICIENCIA) {
	if (is_object($Usuario) && !$Usuario->is_admin) $showform = false;
}

//if(isset($forceShowForm) && $forceShowForm)
	//$showform = 1;


if(strstr($_SERVER['PHP_SELF'],'cdhu') == true ){
    $acao = "obra";
}
if(strstr($_SERVER['PHP_SELF'],'saopaulo') == true || strstr($_SERVER['PHP_SELF'],'rio_de_janeiro') == true || 
		strstr($_SERVER['PHP_SELF'],'faetec') == true || strstr($_SERVER['PHP_SELF'],'uberlandia') == true || 
		strstr($_SERVER['PHP_SELF'],'setelagoas') == true || strstr($_SERVER['PHP_SELF'],'andradas') == true || 
		strstr($_SERVER['PHP_SELF'],'ribeiraopires') == true || strstr($_SERVER['PHP_SELF'],'cepam') == true || 
		$appTransparencia ){
    $acao = "ação";
}

else if ($appMode == APP_TRANSPARENCIA_EFICIENCIA){
	$acao = "conquista";
} else {
	$acao = "opinião";
}

if(!isset($mapCanvasStyle) || $showform == true)
	$mapCanvasStyle = null;


if(!$_USER_FACE_LOGIN)
	$showform = 0;

if($showform)
	$boxFiltroClass = 'colDireita';
else
	$boxFiltroClass = 'colEsquerda';

$id = false;

if ( !empty( $_GET['app_data'] ) ) {
	$id = $_GET['app_data'];
} else if ( !is_null( $json['app_data'] ) ) {
	$id = $json['app_data'];
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

	<?php 
	
	if ( $id ) {
		$q = Doctrine_Query::create()
		->from('Reclamacoes')
		->where('aprovada = "1" AND id = ?', $id );
		 
		$Reclamacao = $q->fetchOne();
		
		$img = $Reclamacao->ilustracao_url;
		if ($Reclamacao->ilustracao_tipo == 'video') {
		
			$lastpart = strstr($img,'&');
			$videoId = str_replace(array('http://www.youtube.com/watch?v=','https://www.youtube.com/watch?v=',$lastpart),'',$img);
		
			$img = 'https://i.ytimg.com/vi/'.$videoId.'/hqdefault.jpg';
			$videoTag = '<meta property="og:video"  content="'.$img.'" />';
		}
		
		if ( empty($img) ) {
			$img = $Reclamacao->ilustracao_url_depois;
		}
		
		?>
		<meta property="og:locale" content="pt_BR" />
		<meta property="og:type" content="place" />
  		<meta property="fb:app_id" content="<?=$facebookAppConfig['appId']; ?>" />
		<meta property="og:url"    content="<?=$appBaseUrl.'?app_data='.$Reclamacao->id?>" /> 
		<meta property="og:title"  content="<?=$Reclamacao->titulo?>" /> 
		<meta property="og:description"  content="<?=$Reclamacao->descricao?>" /> 
		<meta property="og:image"  content="<?=$img?>" /> 
		<meta property="og:image:width"  content="486" />
		<meta property="og:image:height"  content="255" />  
		
		
		<?php 
	
	} else { 
	
		if( isset($metaOGArray) && is_array($metaOGArray) ) {
			$img_share_src = null;
			foreach($metaOGArray as $meta_key => $meta_value ){
				echo '<meta property="'. $meta_key .'" content="'. $meta_value .'"/>';
		
				if($meta_key == "og:image")
					$img_share_src = $meta_value;
			}
		
			if($img_share_src)
				echo '<link rel="image_src" href="'.$img_share_src.'" />';
		}
	}
	
	?>

	<link href="../bootstrap.min.css" rel="stylesheet">
	<link href="../bootstrap-responsive.min.css" rel="stylesheet">

    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" type="text/css" href="../scrollbars.css" />
    <link rel="stylesheet" type="text/css" href="../scrollbars-black.css" />
    <style>
		<?php echo $listIcons; ?>
		<?php echo $appStyle; ?>
    </style>
    
	<script type="text/javascript">
    	//Define Variaveis Iniciais Javascript do Mapa (sera utilizado em googlemap.js
    	var initialLatitude  = <?=$initialLatitude; ?>;
    	var initialLongitude = <?=$initialLongitude; ?>;
    	var boundaryLat1     = <?=$boundaryLat1; ?>;
    	var boundaryLng1     = <?=$boundaryLng1; ?>;
    	var boundaryLat2     = <?=$boundaryLat2; ?>;
    	var boundaryLng2     = <?=$boundaryLng2; ?>;
    	var initialZoom      = <?=$initialZoom; ?>;
    	var minimumZoom      = <?=$minimumZoom; ?>;
    	var cityName  = "<?=$cityName; ?>";
    	var regionCode = "<?=$regionCode; ?>";
    	var userFacebookId = "<?=$user?>";
    	var userEmail = '<?php if(is_object($Usuario)) echo $Usuario->email;?>';
    	var _USER_FACE_LOGIN = "<?php echo $_USER_FACE_LOGIN;?>";
    	var _FACE_LOGIN_URL = "<?php echo $_FACE_LOGIN_URL;?>";

	<?php if(isset($mapCenterConfigLat) && isset($mapCenterConfigLong)){ ?>
		var mapCenterConfigLat = '<?php echo $mapCenterConfigLat;?>';
		var mapCenterConfigLong = '<?php echo $mapCenterConfigLong;?>';
	<?php } else {?>
		var mapCenterConfigLat = null;
		var mapCenterConfigLong = null;
	<?php } ?>
		var naoVota = new Array ();
		<?php if(is_object($Usuario)) {?>
			var userEmail = '<?php echo $Usuario->email;?>';
		<?php } else {?>
			var userEmail = '';
			<?php }?>
	<?php if(isset($naoVota) && is_array($naoVota)){
			foreach($naoVota as $k => $email){
				?> naoVota.push('<?php echo $email;?>');
			<?php }?>
 	<?php }?>

 	
    </script>
    
    <script src="../js/jquery_latest.js"></script>
    <script src="../js/jquery_ui.js"></script>
    <script type="text/javascript" src="https://www.youtube.com/player_api"></script>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxGWbvYkiHpopbuqWMazEAm1n3Rkc7YZY&sensor=false">
    </script>
   	<?php if ($appMode == APP_TRANSPARENCIA_EFICIENCIA){?>
  	  <script src="../js/googlemap_conquista.js"> </script>     
   	<?php } else {?>
	   <script src="../js/googlemap.js"> </script>     
   	<?php }?>
    <script src="../js/dragNdrop.js"> </script>
    <script src="../js/generalscripts.js">	</script> 
    <script src="../js/jquery.event.drag-2.0.min.js"></script>
    <script src="../js/jquery_resize.js"></script>
    <script src="../js/jquery_form.js"></script>
    <script src="../js/mouse_hold.js"></script>
    <script src="../js/mouse_wheel.js"></script>
    <script src="../js/aplweb.scrollbars.js"></script>
    <script src="../js/infobox.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/select-box.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/bootbox.js"></script>
    

    
	<script type="text/javascript">
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '<?=$googleAnalyticsCode?>']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
		function loginFB(closeReclamacaoModal) {
			if(closeReclamacaoModal){
				$('#reclamacoes-container').css('display', 'none');
				$('#mask').css('display', 'none');
			}
			$('#myModal').modal('toggle');
		}
		
	</script>
	<?php if(isset($appTitle)){?>
		<title><?php echo $appTitle;?></title>
	<?php }?>
  </head>
  <body onload="initialize(); window.scroll(0,0);"> 
  
	<div id="fb-root"></div>
	
	<script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '<?=$facebookAppConfig["appId"];?>', // App ID
		  xfbml      : true,
		  version    : 'v2.8'
		});
		FB.AppEvents.logPageView();
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	
	
	
	<script>
	  // window.fbAsyncInit = function() {
	    // FB.init({
	      // appId      : '<?=$facebookAppConfig["appId"];?>', // App ID
	      // channelUrl : 'https://campanhadigital.net.br/aplicativos/channel.html', // Channel File
	      // status     : true, // check login status
	      // cookie     : true, // enable cookies to allow the server to access the session
	      // xfbml      : true  // parse XFBML
	    // });
	  // };
	
	  // (function(d){
	     // var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	     // if (d.getElementById(id)) {return;}
	     // js = d.createElement('script'); js.id = id; js.async = true;
	     // js.src = "https://connect.facebook.net/en_US/all.js";
	     // ref.parentNode.insertBefore(js, ref);
	   // }(document));
	</script>

	<?php 
	if(! isset($HeaderDivStyle))
		$HeaderDivStyle = 'center head-header';
	?>
	  
	<div class="aviso">Cadastro Sucesso</div>     
	
	<header>
		<a href="<?=$headerUrl?>" target="<?php echo $targetHeader;?>">
			<div class="header-top">
				<img src="imgs/header-1400.png">
			</div>
		</a>
	</header>
		
    <div class="menu-mobile">
            <ul>
                <li><a id="participe-mobile" href="#">Cadastre Aqui</a></li>
                <li><a id="filtros-mobile" href="#">Filtros</a></li>
            </ul>
    </div>
    
    <?php if(isset($_BANNER_SUPERIOR_URL) && $_BANNER_SUPERIOR_URL != ''){ 
    	if(!isset($_BANNER_SUPERIOR_LINK) || $_BANNER_SUPERIOR_LINK == '') {
    		$_BANNER_SUPERIOR_LINK = '#';
    		$target = "";
    	} else {
    		$target = " target=\"_blank\"";
    	}
    	
    	?>
    <!-- BANNER SUPERIOR PUBLICIDADE -->
    <a href="<?php echo $_BANNER_SUPERIOR_LINK;?>" <?php echo $target?>>
        <div class="banner-superior">
    	<img src="<?php echo $_BANNER_SUPERIOR_URL;?>">
        </div>	
    </a>	
   
    <?php }?>   

	<div class="container-inst">
	    <div class="instrucao" id="instrucao">
            <h2  <?php echo $appTitleStyle;?> ><?=$appTitle?></h2>
            <p><?=$appDescription?></p> 
	    </div>
    </div>
    
	<div class="recuar">
    	<span class="btn-recuar"></span>
        <span class="recuar-txt">Recolher</span>
	</div>
	     	     
	<?php if(!$_USER_FACE_LOGIN && !isset($_SESSION['fb_id'])) {?>
		<script>
			bootbox.alert("Para acessar o painel administrativo, associe sua conta do Facebook no botão LOGIN acima.");
		</script>
	     <div class="fbLogin" id="fbLogin"><div class="fbLoginInside"><a href="javascript:js;" onclick="parent.location='<?php echo $_FACE_LOGIN_URL;?>'"><img src="../imgs/facebookLogin.png" /></a></div></div>
    <?php } else {?>
		<div id="puxar_participe_aqui"> </div>
	<?php }?>
     <script>
	     var windowWidth = $(window).width();
	     var windowHeight =$(window).height();
	     $('#fbLogin').css({'left':(windowWidth-115) });
     </script>
		
    <div class="participe"> 
    <?php	
	if ($Usuario->is_admin) { ?>  
        <div class="colEsquerda">
            <div class="menu">
            	<h2 class="titulo" id="colEsquerda" style="<?php echo $menuH2Style;?>"><span class="minus-plus"></span><span id="editarCadastrarMinus"> <?php if ($appMode == 2):?>Cadastre Aqui<?else:?>Participe Aqui<?php endif;?></span></h2>
                <div class="formulario" style="<?php echo $menuH2SetaStyle;?>;display:block;">
                    <form id="uploader" action="upload.php" method="post" enctype="multipart/form-data" onsubmit="validaFormulario();">
                    	<input id="endereco" name="endereco" type="text" value="Endereço" original="Endereço" class="first">
                        <div class="tooltip"></div>
                        <input type="hidden" id="reclamacaoId" name="reclamacaoId" value="" original=""/>
                        <input type="hidden" id="usrId" name="usuario" value="<?=$user?>" original=""/>
                        <input type="hidden" id="lat" name="latitude" />
                        <input type="hidden" id="lng" name="longitude" />
                        <input type="hidden" id="sel_autocomplete" name="sel_autocomplete" />
                        <input type="hidden" id="cobertura" name="cobertura" />
                        <div class="styled-select">
                            <select size="1" id="choice" name="categoria">
	                            <?=str_replace("opinião",$acao,$listItems)?>
                            </select>
                        </div>
			<?php
                        if ( false ){?>
							<div id="file_upload" class="file-principal sbHolder">
                                <input type="file" id="fileUploadPrincipal" name="fileUploadPrincipal" />
                                <input type="hidden" id="caminhoArquivoPrincipal" name="nomeArquivoPrincipal" />
                                <input type="hidden" id="tipoUploadPrincipal" name="tipoUploadPrincipal" value="1" />
                                <input type="hidden" name="MAX_FILE_SIZE_PRINCIPAL" value="11000000" />
                                <span class="info-arquivo-principal" original="Selecione uma imagem">Imagem de Marcador</span><span class="button-principal" rel="fileUploadPrincipal"></span>
                                <div id="drop-files-principal" ondragover="return false"> <!-- ondragover for firefox -->
                                    ou solte a imagem aqui
                                </div>
                                
                                <div id="uploaded-holder">
                                    <div id="dropped-files">
                                        <div id="upload-button">	                                            
                                            <a href="#" class="delete">x</a>
                                            <span class="upload">Selecione 1 imagem</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php }?>

                        <input id="titulo" name="titulo" type="text" value="Nome" original="Nome">
                        <input class="telefone" id="telefone" name="telefone" type="text" value="Telefone 000 (00)00000-0000" original="Telefone 000 (00)00000-0000">
                        <input class="email" id="email" name="email" type="text" value="seu@email.com.br" original="seu@email.com.br">
						<textarea id="descricao" name="descricao" original="Conteúdo">Vencimento: <?="\r\n"?>Eleitorado:  <?="\r\n"?>Votações: <?="\r\n"?>Nominata Municipal:  <?="\r\n"?></textarea>

                        <input class="urlFacebook" id="urlFacebook" name="urlFacebook" type="text" value="https://facebook.com/" original="">
                        <input class="urlTwitter" id="urlTwitter" name="urlTwitter" type="text" value="https://twitter.com/" original="">
                        <input class="urlInstagram" id="urlInstagram" name="urlInstagram" type="text" value="https://instagram.com/" original="">
                        <input class="urlWhatsapp" id="urlWhatsapp" name="urlWhatsapp" type="text" value="WhatsApp:55 11 999999999" original="">

                        <ul class="tabnav">  
                            <li class="first active" style="font-size:11px"><b>Imagem</b></li>  
                            <li class="second" style="font-size:11px"><b>Youtube</b></li>                                                          
                        </ul>
                        <div class="form-content">
                            <?php if ($appMode != APP_TRANSPARENCIA_EFICIENCIA){?>
	                            <div id="file_upload" class="file">
	                                <input type="file" id="fileUpload" name="fileUpload" />
	                                <input type="hidden" id="caminhoArquivo" name="nomeArquivo" />
	                                <input type="hidden" id="tipoUpload" name="tipoUpload" value="1" />
	                                <input type="hidden" name="MAX_FILE_SIZE" value="11000000" />
	                                <span class="info-arquivo" original="Selecione uma imagem">Selecione uma imagem</span><span class="button" rel="fileUpload"></span>
	                                <div id="drop-files" ondragover="return false"> <!-- ondragover for firefox -->
	                                    ou solte a imagem aqui
	                                </div>
	                                
	                                <div id="uploaded-holder">
	                                    <div id="dropped-files">
	                                        <div id="upload-button">	                                            
	                                            <a href="#" class="delete">x</a>
	                                            <span class="upload">Selecione 1 imagem</span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>	
                            <?php } else {?>
	                            <div id="file_upload" class="file">
	                                <input type="file" id="fileUpload" name="fileUpload" />
	                                <input type="hidden" id="caminhoArquivo" name="nomeArquivo" />
	                                <input type="hidden" id="tipoUpload" name="tipoUpload" value="1" />
	                                <input type="hidden" name="MAX_FILE_SIZE" value="11000000" />
	                                <span class="info-arquivo" original="Selecione uma imagem" style="color: #4B7214; font-size:11px"><b>Pré-<?=ucfirst($acao)?></b> </span><span class="button" rel="fileUpload"></span>
	                                <div id="drop-files" ondragover="return false"> <!-- ondragover for firefox -->
	                                    ou solte a imagem aqui
	                                </div>
	                                
	                                <div id="uploaded-holder">
	                                    <div id="dropped-files">
	                                        <div id="upload-button">                                                
	                                            <a href="#" class="delete">x</a>
	                                            <span class="upload">Selecione 1 imagem</span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                            <div id="file_upload" class="file-depois">
	                                <input type="file" id="fileUploadDepois" name="fileUploadDepois" />
	                                <input type="hidden" id="caminhoArquivoDepois" name="nomeArquivoDepois" />
	                                <input type="hidden" id="tipoUploadDepois" name="tipoUploadDepois" value="1" />
	                                <input type="hidden" name="MAX_FILE_SIZE_DEPOIS" value="11000000" />
	                                <span class="info-arquivo-depois" original="Selecione uma imagem" style="color: #4B7214; font-size:11px"><b>Pós-<?=ucfirst($acao)?> </b></span><span class="button-depois" rel="fileUploadDepois"></span>
	                                <div id="drop-files-depois" ondragover="return false"> <!-- ondragover for firefox -->
	                                    ou solte a imagem aqui
	                                </div>
	                                
	                                <div id="uploaded-holder">
	                                    <div id="dropped-files">
	                                        <div id="upload-button">                                                
	                                            <a href="#" class="delete">x</a>
	                                            <span class="upload">Selecione 1 imagem</span>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
                            <?php }?>
                            <div id="video_url" class="video">
                                <input name="youtube_url" type="text" value="Digite a URL do vídeo do youtube" original="Digite a URL do vídeo do youtube" />
                            </div>
                        </div>
                        <div id="loading">
                            <div id="loading-bar">
                                <div class="loading-color"> </div>
                            </div>
                            <div id="loading-content">Uploading file.jpg</div>
                        </div>
                        <div id="alertas" class="alertas"></div>
                        <div class="buttons">
                        <!--<span class="button-verde" id="ranking">Ranking</span>-->
                            <input type="submit" id="send" value="Enviar" original="" />
                            <span class="button-verde" id="send-ie" style="display:none">Enviar</span>
                            <img src="../imgs/loading.gif" id="loadinggif" style="display:none;">
                        </div>                        
                    </form>
                    
                </div>                
                <span class="foot-form" ></span>
            </div>             
        </div>
        <div id="showmarker" data-value="true" style="display:none"></div>
     <?php } else {?>
     	<div id="showmarker" data-value="false" style="display:none"></div>
     <?php }?>
     
     <div class="filtros">
        <div class="<?php echo $boxFiltroClass;?>">
            <div class="menu" style="display:block;">
                <h2 class="titulo" id="colDireita" style="background-position: -223px 0px;<?php echo $menuH2Style;?>" ><span class="minus-plus-filtro"></span><span id="FiltroMinus"> Filtros</span></h2>
                <div class="formulario_filtro" style="display: none; <?php echo $menuH2SetaStyle;?>">
                    <form id="filtros" action="index.php" method="get">
                        <?php if(isset($_GET['qtde'])):
                            $qtde = $_GET['qtde'];
                        else:
                            $qtde = 50;
                        endif;?>
                        <input id="qtde" name="qtde" type="hidden" value="<?=$qtde?>" original="2000" class="first">
                        <div class="tooltip"></div>
                        <input type="hidden" id="code" name="code" value="<?=$_GET['code']?>" original=""/>
                        <select size="1" id="categoria_filtro" name="categoria_filtro">
                            <?=str_replace("opinião",$acao,$listItems)?>
                        </select>
                        <?php if(!isset($_GET['data_inicio']))
                            $_GET['data_inicio'] = 'Escolha uma data de início';
                            if(!isset($_GET['data_fim']))
                            $_GET['data_fim'] = 'Escolha uma data máxima';?>
<!--
                        <input id="data_inicio" name="data_inicio" type="text" value="<?=$_GET['data_inicio']?>" original="Escolha uma data de início">
                        <input id="data_fim" name="data_fim" type="text" value="<?=$_GET['data_fim']?>" original="Escolha uma data máxima">
-->
                        <div class="buttons" style="border-top:none;">
                        	<span class="button-cinza" id="cleanFilter" onclick="document.location ='<?php echo $appBaseUrl;?>';" >Voltar</span>
                            <input type="submit" id="send" value="Filtrar" original="" />
                            <span class="button-verde" id="send-ie" style="display:none">Filtrar</span>
                        </div>                        
                    </form>
                    
                </div>                
                <span class="foot-form" ></span>
            </div>             
        </div>
     </div>
        <div class="social-phone">   
                <!-- Redes Sociais -->
                <div class="compartilhe">
                <div class="addthis_toolbox addthis_default_style addthis_32x32_style mgl-10">
                 <a class="addthis_button_preferred_1"></a>
                <a class="addthis_button_preferred_2"></a>
                <a class="addthis_button_preferred_3"></a>
                <a class="addthis_button_preferred_4"></a>
                <a class="addthis_button_compact"></a>
                </div><!-- Fim Redes Sociais -->     
                </div>   
		</div>
 	</div><!-- FIM INTERAÇÃO -->

 	<div id="container">  
        <div id="map_canvas" style="z-index:0;"></div>          
    </div>
    
    <!-- FIM DO CONTAINER -->
    <a id="btn_back" href="#topo">
    	<div class="subir"></div>
    </a>
        
	<div id='mask' class='close-modal'></div>
     
    <!-- MODAL DO RANKING -->
	<div id="ranking-container" class='modal-window ranking'>
            	<div id="modal-content">
                    <ul class="header-table">
                    	<li>Posição</li>
                        <li>Autor</li>
                        <li><?=$acao?></li>
                        <li>Apoios</li>
                    </ul>
                    <div class="contain">
                    	<div class="tripa">

                            <ul class="row">
                            	<li class="position">
                                </li>
                                <li class="autor">
                                </li>
                                <li class="reclamation"> 
                                </li>
                                <li class="votos"> 
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
	</div>

            
        <!-- MODAL COM DETALHES DA opinião -->
    <div id="reclamacoes-container" class="modal-window amplia" >

		
                <!-- IMAGEM E DESCRIÇÃO -->
                <div id="contentL">
                    <div class="uploadedFile"><!-- <img src="imgs/uploaded/buracos.png" width="381" height="254"> --></div>
                    <div class="uploadedContent">

                      <div class="resumo">
                            <div class="conteudo" style="height: 72px;overflow:auto">
                            </div>
                        <div id="popup" class="buttons">
                                <span id="votar" class="button-verde">Apoiar</span>
                                <span id="share" class="button-verde">Compartilhar</span>
                            </div>
                      </div>  
                    </div>
                </div>
				
				
                
              <?php if ($showform) {?>
              <!-- CAIXA DE COMENTARIOS DA MODAL -->
	
              <div id="contentR">
                    <div id="comentarios" style="overflow: scroll;height: 60%;">
                        <ul class="comments">
                        </ul>
                  	</div>
                    <div id="makeComent" class="invert-coment">
                      <ul class="comments">
                      </ul>
                  	</div>                  	
              </div> 
              <?php }?>
              
              
	</div>

    
    <img src="images/bolinha-logo.png" style="position:fixed;bottom:10px;right:10px;cursor:pointer" />

<?php 
// if ($appMode == APP_GABINETE_INTERATIVO || $appMode == APP_MODE_ZERO || ($appMode == APP_TRANSPARENCIA_EFICIENCIA && (is_object($Usuario) && $Usuario->is_admin != 0))) {
?>
    <div id="dialog-message-ok" title="Marcação de endereço">
    <p>
    <br>
    Você definiu o endereço <b><span class="endereco_msg"></span></b> e ele será relacionado ao ponto definido no mapa identificado pelo icone abaixo:<br>
    <br>
    <span style="float: left; margin-left: 109px;margin-right:129px"><img src="imgs/redmarker.png" border="0"></span>
    <br><br>
    Você pode alterar a posição ou a descrição do endereço a qualquer momento antes de pressionar o botão "enviar".
    </p>
    </div>

    <div id="dialog-message" title="Marcação de endereço">
    <p class="boxdialog" style="width:320px;">
    <span style="float: left; margin: 40px 7px 50px 0;"><img src="imgs/redmarker.png" border="0"></span>
    <br><br>
    Utilize o marcador disposto no mapa para marcar o endereço que deseja.<br>
    <br>
    O aplicativo tentará preencher o campo endereço automaticamente mas em áreas onde as ruas não são informadas, você mesmo pode marcar e escrever o endereço.
    </p>
    </div>    
<?php 
// }
?>


    <?php     
    
    $signed_request = explode('.', $_POST['signed_request']);  
    $json = json_decode(base64_decode($signed_request['1']), true);
    
    if ( $id ) { ?>
    	<script type="text/javascript" >
    		$(document).ready(function () {
    			load_modal("reclamacoes-container","amplia","place.php?ajax=1&user=<?=$user?>&id=<?=$id?>");
    		});
    	</script>
    <?php } ?>
  </body>
  
<div id="myModal" class="modal fade">
	<div class="modal-dialog">
	<div class="modal-content">
	<!-- dialog body -->
	<div class="modal-body">
	<button type="button" class="close" onclick="loginFB();">&times;</button>
	Para esta funcionalidade você deve estar logado no Facecbook.<br><br>Clique no botão abaixo para
	se logar no Facebook.
	</div>
	<!-- dialog buttons -->
	<div class="modal-footer"><a href="<?php echo $_FACE_LOGIN_URL;?>"><img src="../imgs/facebookLogin.png" /></a></div>
	</div>
	</div>
</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.12/jquery.mask.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.telefone').mask('000 (00) 00000-0000');
    });
</script>


<script>

$("#myModal").modal({ // wire up the actual modal functionality and show the dialog
	"backdrop" : "static",
	"keyboard" : true,
	"show" : false// ensure the modal is shown immediately
});


<?php if(isset($_BANNER_SUPERIOR_URL) && $_BANNER_SUPERIOR_URL != ''){ ?>
$('.participe').css('top', '380px');
$('.social-icons').css('top', '380px');
$('.fbLoginInside').css('top', '200px');
<?php }?>


<?php if(isset($_GET['facebookLogin']) && $_GET['facebookLogin'] == 'true' && $_USER_FACE_LOGIN) {?>

$( document ).ready(function() {
	
		var width=$(window).width();
		
		// if( width > 420 ) {
			// bootbox.alert("Você agora está logado no Facebook. Poderá realizar curtir, comentar e compartilhar.");
		// }

		<?php if(isset($_GET['redirect_app']) && $_GET['redirect_app'] == 'true' && $_USER_FACE_LOGIN) { ?>
		var url = "<?php echo $facebookBaseUrl; ?>";
		$(location).attr('href',url);
		<?php } ?>
});

<?php }


$__PHP_MEMMORY_END = memory_get_usage();
$__TIME_END = time();

if(isset($__TEST_PERFORMANCE) && $__TEST_PERFORMANCE == true) {?>
$( document ).ready(function() {

	var memStart = '<?php echo ($__PHP_MEMMORY_START / 1024 / 1024)?>';
	var memLoad = '<?php echo ($__PHP_MEMMORY_LOAD / 1024 / 1024)?>';
	var memEnd = '<?php echo ($__PHP_MEMMORY_END / 1024 / 1024)?>';
	var totalTime = '<?php echo ($__TIME_END - $__TIME_START);?>';
	bootbox.alert("start: "+ memStart + " load: " + memLoad + " memEnd: " + memEnd + " total sec: " + totalTime);
	
/*var width=$(window).width();
if(width > 790 && width < 860)*/
	//$('.header-top').css({background: 'url("imgs/header.png") no-repeat scroll center top rgba(0, 0, 0, 0)'});
});
<?php }?>
</script>

</html>
