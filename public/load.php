<?php
$__START_TIME = time();
session_start();


// ini_set( 'display_errors', 1 ) ;
// ini_set( 'display_startup_errors', 1 ) ;
// error_reporting( E_ALL ) ;
// echo "<pre>" ; print_r( $_POST ) ; echo "</pre>" ;


/* load */
// require_once "user.php";

// begin: ajuste 04042017
require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/App.php';
require_once $appName.'/config/Database.php';
/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');

// O usuário existe no banco?
$u = Doctrine_Query::create()
        ->from('Usuarios')
        ->where('facebook_id = ?', $_SESSION['fb_id']);

$Usuario = $u->fetchOne();

/* DOCTRINE ***************************************************************/
// end: ajuste 04042017

$q = Doctrine_Query::create()
        ->from('Reclamacoes')
        ->where('aprovada = ?','1');
//@WCS
// quantidade de limite
// if($_GET['qtde']){
		// $q->limit($_GET['qtde']);
// }

if($_GET['categoria_filtro']) {
	$q->andwhere('categoria = "'.$_GET['categoria_filtro'].'"');
}

if($_GET['data_inicio'] != 'Escolha uma data de início' && $_GET['data_inicio'] != '') {
	$data_inicio = explode('/',$_GET['data_inicio']);
	$_GET['data_inicio'] = $data_inicio['2'].'-'.$data_inicio['1'].'-'.$data_inicio['0'];
//	$q->where('aprovada_em = ?',);	
	$q->andwhere("aprovada_em > '" . $_GET['data_inicio'] . "'");
}

if($_GET['data_fim'] != 'Escolha uma data máxima' && $_GET['data_fim'] != '') {
	$data_fim = explode('/',$_GET['data_fim']);
	$_GET['data_fim'] = $data_fim['2'].'-'.$data_fim['1'].'-'.$data_fim['0'];
	$q->andwhere("aprovada_em < '" 	. $_GET['data_fim'] . "'");
}

$q->orderBy('aprovada_em', 'desc');
$Reclamacoes = $q->execute();

$matriz = array();
foreach ($Reclamacoes as $reclamacao) {
	
	$q = Doctrine_Query::create()
                ->from('Votos')
                ->where('reclamacao_id = ?', $reclamacao->id);

	$Votos = $q->execute();
	$votosCount = count($Votos);
	$podeVotar = '1';
	
	if( !isset( $_SESSION['fb_id'] ) ) $podeVotar = '2' ;
	
	foreach ($Votos as $voto) {
		// if ($voto->usuario_id == $user) $podeVotar = '0';
		if ($voto->usuario_id == $_SESSION['fb_id']) $podeVotar = '0';
	}
	
	// if (!$user) $podeVotar = '0';
		
	$img = $reclamacao->ilustracao_url;
	$img_depois = $reclamacao->ilustracao_url_depois;
	$img_principal = $reclamacao->ilustracao_url_principal;
	
	if ($reclamacao->ilustracao_tipo == 'video') {
	
		$lastpart = strstr($img,'&');
		$videoId = str_replace(array("http://www.youtube.com/watch?v=","https://www.youtube.com/watch?v=",$lastpart),"",$img);		
		
		$img = "https://i.ytimg.com/vi/".$videoId."/hqdefault.jpg";
		
	}
		
	if(is_object($Usuario) && $Usuario->is_admin){
		$is_admin = 1;
	} else {
		$is_admin = null;
	}

	$__PHP_MEMMORY_END = memory_get_usage()/1024/1024;
	$__END_TIME = time();	
	$matriz[] = array('id'        =>$reclamacao->id, 
			  'latitude'  =>$reclamacao->latitude, 
			  'longitude' =>$reclamacao->longitude, 
			  'fbpicture' =>"https://graph.facebook.com/".$reclamacao->usuario_id."/picture",
			  'endereco'  =>$reclamacao->endereco,
			  'titulo'    =>$reclamacao->titulo,
			  'descricao' =>$reclamacao->descricao,
			  'tipo'      =>$reclamacao->ilustracao_tipo,
			  'estado'	  =>$reclamacao->estado_conquista,
			  'imagem'    =>$img,
			  'imagem_depois' =>$img_depois,
			  'imagem_principal' =>$img_principal,
			  'votos'     =>$votosCount,
			  'categoria' =>$reclamacao->categoria,
			  'podevotar' =>$podeVotar,
			  'is_admin'  =>$is_admin,
			  'fb_id'     => $reclamacao->usuario_id,
			  'memory_usage' => $__PHP_MEMMORY_END,
			  'total_load_time' => $__END_TIME - $__START_TIME,
			  'cargo' => $cats[$reclamacao->categoria],

			  'urlFacebook' => $reclamacao->urlFacebook,
			  'urlTwitter' => $reclamacao->urlTwitter,
			  'urlInstagram' => $reclamacao->urlInstagram,
			  'urlWhatsapp' => $reclamacao->urlWhatsapp,

			  'telefone' => $reclamacao->telefone,
			  'email' => $reclamacao->email,


			  );
	
}

echo json_encode($matriz);
// echo "<pre>" ; print_r( $matriz ) ; echo "</pre>" ;