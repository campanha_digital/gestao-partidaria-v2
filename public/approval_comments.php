<?php


// ini_set( 'display_errors', 1 ) ;
// ini_set( 'display_startup_errors', 1 ) ;
// error_reporting( E_ALL ) ;

session_start();


/* approval */
// require 'user.php';


// echo "<pre>" ; print_r( $_SESSION ) ; echo "</pre>" ;


require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/Database.php';
require_once $appName.'/config/App.php';

/* DOCTRINE ***************************************************************/

spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');

$u = Doctrine_Query::create()
        ->from('Usuarios')
        ->where('facebook_id = ?', $_SESSION['fb_id']);

$Usuario = $u->fetchOne();


/* FACEBOOK ***************************************************************/

//echo "<pre>";
//print_r($Usuario);

if(!isset($_GET['counter']))
	$counter = 1;
else
	$counter = $_GET['counter'] + 1;

if($counter > 5)
	$reTry = false;
else
	$reTry = true;

// if(!$user && !isset($approvalLogin)) {
	// if(isset($_GET['aprovada']))
		// $aprovada = 'aprovada='.$_GET['aprovada'];
	// else
		// $aprovada = null;
	// $approvalLogin =  $facebook->getLoginUrl(array('scope'=>$fbPermissions,'redirect_uri'=>$appBaseUrl.'approval_comments.php?counter='.$counter.'&'.$aprovada));
// }

// if(!$user && $reTry) {
	// header('Location: '.$approvalLogin);
// }

if (!$Usuario) die("Ocorreu um erro ao tentar identificar seu usu&aacute;rio. Tente carregar o APP fora do Approval e volte para esta p&aacute;gina novamente.");

if (!$Usuario->is_admin) die('Voc&ecirc; n&atilde;o possui autoriza&ccedil;&atilde;o para ver essa p&aacute;gina.');

if (isset($_GET['comentario_id']) && isset($_GET['value'])) {
	$q = Doctrine_Query::create()
	        ->from('Comentarios')
	        ->where('id = ?', $_GET['comentario_id']);
	        
	$Reclamacao = $q->fetchOne();
	
	$Reclamacao->aprovado = $_GET['value'];
	$Reclamacao->save();

	switch($_GET['value']){
		case 0:
			$_acao = 'publicado';
			break;
		case 1:
			$_acao = 'aprovado';
			break;
		case 2:
			$_acao = 'excluído';
			break;
	}
	echo "Comentario foi $_acao com sucesso<br>";	
}



// $aprovada = '1';
// if (isset($_GET['aprovada'])) $aprovada = $_GET['aprovada'];


// if ($aprovada == '1') $titulo = "Comentários Publicados";
$q = Doctrine_Query::create()
        ->from('Comentarios c')
        ->where('aprovado = ?','1')
        ->orderBy('id DESC');

$Reclamacoes = $q->execute();

if (empty($headerImg)) $headerImg = 'header.png';

$q = Doctrine_Query::create()
->from('Comentarios c')
->where('aprovado = ?','0')
->orderBy('id DESC');

$Reclamacoes_pendentes = $q->execute();



?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../style.css" />
    <script src="js/jquery_latest.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
	
	<?=$appStyle?>
    </style>

  </head>
  <body> 


  <header>
	
	    	<div class="<?php echo $HeaderDivStyle;?>">
		<?php if(isset($headerImg) && $headerImg != 'none') {?>
		<a href="<?=$headerUrl?>" target="_top">
		<img src="imgs/<?php echo $headerImg; ?>" ></a>
		<?php } ?>
		</div>
	
    </header>
<br><br>


<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?=$facebookAppConfig["appId"];?>', // App ID
      channelUrl : '//campanhadigital.net.br/aplicativos/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

  <table border="0" align="center"><tr><td>
		<a href="approval.php?aprovada=0"><img src="../imgs/reclamacoespendentes.png" border="0"></a><br>
		</td><td>
		<a href="approval.php?aprovada=1"><img src="../imgs/reclamacoespublicadas.png" border="0"></a><br>
		</td>
		<td>
		<a href="administradores.php"><img src="../imgs/administradoresbotao.png" border="0"></a><br>
		</td>
		</tr>
		<tr><td>
		<a href="approval.php?aprovada=2"><img src="../imgs/reclamacoesrejeitadas.png" border="0"></a><br>
		</td><td  colspan="2">
		<a href="approval_comments.php"><img src="../imgs/vercomentarios.png" border="0"></a><br>
		</td></tr>
		 </table> 
  		<h1>Comentários Publicados</h1>
  		
		<br>
		
		<?php 
		if (count($Reclamacoes) == 0) {
			echo "<strong>Não existem ".$titulo."</strong>";
		} else { ?>
		
		<table class="approval" border="0"  align="center">
		<tr>
			<th>
			Usuário:
			</th>
			<th>
			Reclamação:
			</th>
			<th>
			Foto ou Vídeo:
			</th>
			<th>
			Ação:
			</th>
		</tr>
		
		<?php
		foreach ($Reclamacoes as $reclamacao) {
		
			$q = Doctrine_Query::create()
	        ->from('Reclamacoes')
	        ->where('id = ?',$reclamacao->reclamacao_id);

			$Recl = $q->fetchOne();
			//$img = $reclamacao->ilustracao_url;
	
			/*if ($reclamacao->ilustracao_tipo == 'video') {
			
				$lastpart = strstr($img,"&");
				$videoId = str_replace(array("http://www.youtube.com/watch?v=","https://www.youtube.com/watch?v=",$lastpart),"",$img);		
				
				$videoEmbed = "https://www.youtube.com/embed/".$videoId;
				
			}*/
			?>
			<tr>
				<td>
					<a href="https://facebook.com/<?=$reclamacao->usuario_id ?>" >
					<img src="https://graph.facebook.com/<?=$reclamacao->usuario_id ?>/picture" />
					</a>
				</td>
				
				<td>
					<strong><?=$Recl->titulo?></strong> 
				</td>
				
				<td>
					<strong><?=$reclamacao->texto?></strong> 

					<?php /*if ($reclamacao->ilustracao_tipo == 'foto') { ?>
						<img width="320" src="<?=str_replace('http://','https://',$reclamacao->ilustracao_url)?>" />
					<?php } else { ?>
						<iframe src="<?=$videoEmbed?>" frameborder="0" allowfullscreen></iframe>
					<?php }*/ ?>
				</td>
				
				<td>
					<!--<a style="color:#00ff00;margin-bottom:10px" href="approval.php?reclamacaoid=<?=$reclamacao->id?>&value=1" >Aprovar e publicar esta reclamação</a> <br><br>-->
					<a style="color:#ff0000;margin-bottom:10px" href="approval_comments.php?comentario_id=<?=$reclamacao->id?>&value=2" >Excluir este comentário</a> 					
				</td>
				
				<?php /*
				if ($aprovada == 1) { ?>
					<td>
						Aprovada por: <br>
						<a href="https://facebook.com/<? $reclamacao->aprovada_por ?>" >
						<img src="https://graph.facebook.com/<?=$reclamacao->aprovada_por ?>/picture" />
						</a>
						<br>
						Na data:<br>
						<?=$reclamacao->aprovada_em?>
					</td>
				<?php
				}
				?>
				
				<?php
				if ($aprovada == 2) { ?>
					<td>
						Rejeitada por: <br>
						<a href="https://facebook.com/<?=$reclamacao->aprovada_por ?>" >
						<img src="https://graph.facebook.com/<?=$reclamacao->aprovada_por ?>/picture" />
						</a>
						<br>
						Na data:<br>
						<?=$reclamacao->aprovada_em?>
					</td>
				<?php
				}*/
				?>
				
			</tr>
		<?php } ?>
		</table>
		<?php } ?>
		
		
		<hr>
		
			<h1>Comentários Pendentes</h1>
  		
		<br>
		
		<?php 
		if (count($Reclamacoes_pendentes) == 0) {
			echo "<strong>Não existem ".$titulo."</strong>";
		} else { ?>
		
		<table class="approval"  border="0">
		<tr>
			<th>
			Usuário:
			</th>
			<th>
			Reclamação:
			</th>
			<th>
			Foto ou Vídeo:
			</th>
			<th>
			Ação:
			</th>
		</tr>
		
		<?php
		foreach ($Reclamacoes_pendentes as $reclamacao) {
		
			$q = Doctrine_Query::create()
	        ->from('Reclamacoes')
	        ->where('id = ?',$reclamacao->reclamacao_id);

			$Recl = $q->fetchOne();
			//$img = $reclamacao->ilustracao_url;
	
			/*if ($reclamacao->ilustracao_tipo == 'video') {
			
				$lastpart = strstr($img,"&");
				$videoId = str_replace(array("http://www.youtube.com/watch?v=","https://www.youtube.com/watch?v=",$lastpart),"",$img);		
				
				$videoEmbed = "https://www.youtube.com/embed/".$videoId;
				
			}*/
			?>
			<tr>
				<td>
					<a href="https://facebook.com/<?=$reclamacao->usuario_id ?>" >
					<img src="https://graph.facebook.com/<?=$reclamacao->usuario_id ?>/picture" />
					</a>
				</td>
				
				<td>
					<strong><?=$Recl->titulo?></strong> 
				</td>
				
				<td>
					<strong><?=$reclamacao->texto?></strong> 

					<?php /*if ($reclamacao->ilustracao_tipo == 'foto') { ?>
						<img width="320" src="<?=str_replace('http://','https://',$reclamacao->ilustracao_url)?>" />
					<?php } else { ?>
						<iframe src="<?=$videoEmbed?>" frameborder="0" allowfullscreen></iframe>
					<?php }*/ ?>
				</td>
				
				<td>
					<!--<a style="color:#00ff00;margin-bottom:10px" href="approval.php?reclamacaoid=<?=$reclamacao->id?>&value=1" >Aprovar e publicar esta reclamação</a> <br><br>-->
					<a style="color:#ff0000;margin-bottom:10px" href="approval_comments.php?comentario_id=<?=$reclamacao->id?>&value=1" >Aprovar este comentário</a> 					
				</td>
				
				<?php /*
				if ($aprovada == 1) { ?>
					<td>
						Aprovada por: <br>
						<a href="https://facebook.com/<? $reclamacao->aprovada_por ?>" >
						<img src="https://graph.facebook.com/<?=$reclamacao->aprovada_por ?>/picture" />
						</a>
						<br>
						Na data:<br>
						<?=$reclamacao->aprovada_em?>
					</td>
				<?php
				}
				?>
				
				<?php
				if ($aprovada == 2) { ?>
					<td>
						Rejeitada por: <br>
						<a href="https://facebook.com/<?=$reclamacao->aprovada_por ?>" >
						<img src="https://graph.facebook.com/<?=$reclamacao->aprovada_por ?>/picture" />
						</a>
						<br>
						Na data:<br>
						<?=$reclamacao->aprovada_em?>
					</td>
				<?php
				}*/
				?>
				
			</tr>
		<?php } ?>
		</table>
		<?php } ?>
	
  </body>
</html>
