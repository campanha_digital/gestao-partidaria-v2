<?php
$cats = array(
	'diretorio-nacional'=>'Diretório Nacional',
	'diretorio-estadual'=>'Diretório Estadual',			  
	'diretorio-municipal'=>'Diretório Municipal',
	'comissao-provisoria'=>'Comissão Provisória', 
	'ministro'=>'Ministro', 
	'secretario-nacional'=>'Secretário Nacional', 
	'governador'=>'Governador',
	'vice-governador'=>'Vice-Governador',
	'secretario-estadual'=>'Secretário Estadual',
	'prefeito'=>'Prefeito', 
	'vice-prefeito'=>'Vice-Prefeito', 
	'secretario-municipal'=>'Secretário Municipal', 
	'senador'=>'Senador',
	'deputado-federal'=>'Deputado Federal',
	'deputado-estadual'=>'Deputado Estadual',
	'vereador'=>'Vereador',
	'autarquias-estatais'=>'Autarquias e Estatais',
	'outros'=>'Outros');

$listIcons = '
	.sbOptions li:nth-child(1) { display:none;  }
	.sbOptions li:nth-child(2)  a{ background: url("../imgs/icon_diretorio-nacional.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(3)  a{ background: url("../imgs/icon_diretorio-estadual.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(4)  a{ background: url("../imgs/icon_diretorio-municipal.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(5)  a{ background: url("../imgs/icon_comissao-provisoria.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(6)  a{ background: url("../imgs/icon_ministro.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(7)  a{ background: url("../imgs/icon_secretario-nacional.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(8)  a{ background: url("../imgs/icon_governador.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(9)  a{ background: url("../imgs/icon_vice-governador.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(10) a{ background: url("../imgs/icon_secretario-estadual.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(11) a{ background: url("../imgs/icon_prefeito.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(12) a{ background: url("../imgs/icon_vice-prefeito.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(13) a{ background: url("../imgs/icon_secretario-municipal.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(14) a{ background: url("../imgs/icon_senador.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(15) a{ background: url("../imgs/icon_deputado-federal.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(16) a{ background: url("../imgs/icon_deputado-estadual.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(17) a{ background: url("../imgs/icon_vereador.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(18) a{ background: url("../imgs/icon_autarquias-estatais.png") no-repeat scroll 10px center transparent;  }
	.sbOptions li:nth-child(19) a{ background: url("../imgs/icon_outros.png") no-repeat scroll 10px center transparent;  }
';

$comentBox = true;

$app_arr = explode('/', $appName);
$appName = $app_arr[count($app_arr) - 1];
$rootDir = str_replace($appName, '', dirname($_SERVER['SCRIPT_FILENAME']));

chdir($rootDir);

/* APP ********************************************************************/
$appBaseUrl = 'https://campanhadigital.net.br/sgp/psd/';
// $appBaseUrl = 'http://localhost/psd/';
$uploadDir  = dirname(__DIR__).'/imgs/uploaded/';
$appNamespace = 'gestaopartidaria';
$facebookBaseUrl = 'https://apps.facebook.com/pb-geo';
$headerUrl = $appBaseUrl;

$appMode = 2;
$appTransparencia = true;
//$__TEST_PERFORMANCE = false;

$allowedFileExtensions = array('jpg','jpeg','png','gif','bmp','tga');

$appTitle = 'Planejamento e gestão das lideranças e estrutura do Partido Social Democrático';
$appTitleStyle = ' style="color:#2C41AD;" ';
$appDescription = '';

$appStyle = "header { height: 160px; width:100%; background:url(imgs/bg_header.png) repeat; position:relative; z-index:1; }";
$headerImg = 'header.png';
$HeaderDivStyle = "nothing";

$menuH2Style = ' background:url(\'imgs/bg_tit_menu.png\')  ';
$menuH2SetaStyle = ' background: url(\'imgs/seta.png\') no-repeat scroll center top #ffffff; ';

$mapCanvasStyle=' min-height: 700px; ';


/* ITENS DA LISTA *********************************************************/
if(!isset($_GET['categoria_filtro']))
	$selFiltro = 'selected="selected"';
else
	$selFiltro = '';

if(!isset($_GET['categoria_filtro'])){
	$selFiltro = 'selected="selected"';
} else {
	$selFiltro = '';
}

$listItems = '<option value="" '.$selFiltro.' name="Categoria">Categoria</option>';

foreach ($cats as $value => $desc) {
	if(isset($_GET['categoria_filtro']) && $_GET['categoria_filtro'] == $value){
		$selected = 'selected = "selected"';
	} else {
		$selected = '';
	}
	$listItems .= '<option value="'.$value.'" '.$selected.'>'.$desc.'</option>';
}

/* MAPA *******************************************************************/
$initialLatitude = -23.6011523;
$initialLongitude = -46.714115;
$mapCenterConfigLat = $initialLatitude;
$mapCenterConfigLong = $initialLongitude;
$boundaryLat1 = $initialLatitude - 40.23000;
$boundaryLng1 = $initialLongitude - 40.23000;
$boundaryLat2 = $initialLatitude + 40.23000;
$boundaryLng2 = $initialLongitude + 40.23000;
$initialZoom  = 7;
$minimumZoom  = 5;
$cityName     = "";
$regionCode   = "";

/* FACEBOOK ***************************************************************/
$facebookAppConfig = array(
		'appId'  => '149112349036421',
		'secret' => 'ac7013df363d461453d6236ad3470872',
		'redirect_uri' => $appBaseUrl.'?facebookLogin=true'
);

$appPermissions = array('scope'=>'email,user_hometown,user_likes,user_location');
// $appPermissions = array('scope'=>'email,user_about_me,user_hometown,publish_actions,user_likes,user_location,user_relationships,user_relationship_details,user_religion_politics,publish_actions');
$appPostMessage = 'Planejamento e gestão das lideranças e estrutura do Partido Social Democrático';
$appSharePrompt = 'Planejamento e gestão das lideranças e estrutura do Partido Social Democrático';

$tweeterShareText = 'Planejamento e gestão das lideranças e estrutura do Partido Social Democrático ';

$metaOGArray = array(
		"og:image" => $appBaseUrl . '/imgs/compartilhamento.png',
		'og:image:secure_url' => $appBaseUrl . '/imgs/compartilhamento.png',
		'og:title' => $appTitle,
		'og:description' => 'Planejamento e gestão das lideranças e estrutura do Partido Social Democrático'
);

/* GOOGLE ANALYTICS E DEMAIS SERVIÇOS *************************************/
$googleAnalyticsCode = 'UA-33656101-12';
