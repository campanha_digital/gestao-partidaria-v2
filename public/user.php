<?php 
/* user */
// echo "<pre>" ; print_r( 'user.php' ) ; echo "</pre>" ;

// session_start();

//login do face acontece no bootstrap. Boostrap oferece variavel $user
require_once 'bootstrap.php';

// O usuário existe no banco?

$fb_id = isset($_SESSION['fb_id']) ? $_SESSION['fb_id'] : 0;
$fb_email = isset($_SESSION['fb_email']) ? $_SESSION['fb_email'] : '';

$q = Doctrine_Query::create()
        ->from('Usuarios')
        ->where('email = ?', $fb_email);
        // ->where('facebook_id = ?', $fb_id);

$Usuario = $q->fetchOne();

if(is_object($facebook)) {
	$facebook->setExtendedAccessToken();
	$token = $facebook->getAccessToken();
} else {
	$token = null;
}



// echo "<pre>" ; print_r( $Usuario ) ; echo "</pre>" ;


if ($Usuario) { // Usuario logado no face e encontrado na base
    // if ($token && $token != $Usuario->facebook_token) {
    	$Usuario->facebook_token = $token;
		$Usuario->nome =  $_SESSION['fb_user_profile']['name'];
		$Usuario->email = $_SESSION['fb_email'];
		$Usuario->profile_info = json_encode($user_profile);
		$Usuario->save();

    // }
} else { // User nao esta logado no face ou nao foi encontrado no face

	// if (!empty($user) && isset($_SESSION['fb_user_profile'])) // Garante que user esta logado no face
	if (!empty($user) && isset($_SESSION['fb_id'])) // Garante que user esta logado no face
	{
	    $Usuario = new Usuarios();
		$Usuario->facebook_id = $_SESSION['fb_id'];
		$Usuario->facebook_token = $_SESSION['fb_token'];
		$Usuario->nome = $_SESSION['fb_user_profile']['name'];
		$Usuario->created_at = new Doctrine_Expression('NOW()');
		$Usuario->email = $_SESSION['fb_email'];
		$Usuario->profile_info = json_encode($user_profile);
		// $Usuario->profile_info = $_SESSION['fb_link'];
		
		if(isset($adminEmails)){
			if(in_array($_SESSION['fb_email'], $adminEmails))
				$Usuario->is_admin = true;
		}
		
		$Usuario->save();
	}
}

// echo "<pre>" ; print_r( $Usuario ) ; echo "</pre>" ;
