<?php
require_once "bootstrap.php";
require_once "user.php";


$QTD_MARCACOES = 1000;
$USER_ID = '289203321290550';
$ENDERECO = 'Rua Sete de Março, 1-33, Ribeirão do Sul - SP, 19930-000, Brazil';
$CATEGORIA = 'meio_ambiente';
$DESCRICAO = 'teste';
$TITULO = 'teste';
$LAT = -22.787007872996185;
$LONG = -49.935079465637216;
$TIPO_APP = 1;
$DATA = '01/01/2005';

for($i = 0 ; $i < $QTD_MARCACOES; $i++){

	$_POST['usuario'] =$USER_ID;
	$_POST['endereco'] ='$ENDERECO';
	$_POST['latitude'] =$LAT ;
	$_POST['longitude'] =$LONG ;
	$_POST['categoria'] = $CATEGORIA;
	$_POST['titulo'] =$TITULO." - $i";
	$_POST['descricao'] =$DESCRICAO." - $i";
	$_POST['estado_conquista'] = $TIPO_APP;
	$_POST['data_realizacao'] = $DATA;
	
	/*$googleApiUrl = "https://maps.googleapis.com/maps/api/streetview?size=381x257&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=90&heading=235&pitch=10&sensor=false";
	$isYoutubeUrl = ( !empty($_POST['youtube_url']) && ( strpos($_POST['youtube_url'], 'youtube.com') !== false ) );
	
	if ( $_POST['tipoUpload'] == 1  && empty($_POST['nomeArquivo']) ) $fileUrl = $googleApiUrl;
	if ( $_POST['tipoUpload'] == 2  && !$isYoutubeUrl ) $fileUrl = $googleApiUrl;
	
	if ( $_POST['tipoUploadDepois'] == 1  && empty($_POST['nomeArquivoDepois']) ) $fileUrlDepois = $googleApiUrl;
	if ( $_POST['tipoUploadDepois'] == 2  && !$isYoutubeUrl ) $fileUrlDepois = $googleApiUrl;
	
	$isBase64EncodedImage = ( empty($_FILES) && substr($_POST['nomeArquivo'],0,11) == 'data:image/' );
	$isBase64EncodedImageDepois = ( empty($_FILES) && substr($_POST['nomeArquivoDepois'],0,11) == 'data:image/' );
	$isBase64EncodedImagePrincipal = ( empty($_FILES) && substr($_POST['nomeArquivoPrincipal'],0,11) == 'data:image/' );
	$isUploadedImage      = (!empty($_FILES) && substr($_FILES['fileUpload']['type'],0,6) == 'image/');
	$isUploadedImageDepois      = (!empty($_FILES) && substr($_FILES['fileUploadDepois']['type'],0,6) == 'image/');
	$isUploadedImagePrincipal      = (!empty($_FILES) && substr($_FILES['fileUploadPrincipal']['type'],0,6) == 'image/');
	
	if ($isBase64EncodedImage) {
		$extension = substr( $_POST['nomeArquivo'], 11, 3 );
		if ($extension == 'jpe') $extension = 'jpeg';
		if (!in_array($extension,$allowedFileExtensions)) echo('BASE64: O arquivo precisa ser uma imagem');
	
		$base64str = str_replace('base64,','',stristr($_POST['nomeArquivo'],';base64,'));
		$imageData = base64_decode($base64str);
	
		$filename = $_POST['usuario'] . '_' . time() . '.' . $extension;
	
		$uploadedFile = $uploadDir . $filename;
		if (!file_put_contents($uploadedFile, $imageData)) echo('BASE64: Ocorreu um erro ao salvar a imagem.');
	
	
	} else if($isUploadedImage) {
		$extension = substr( $_FILES['fileUpload']['type'] , -3 );
		if ($extension == 'peg') $extension = 'jpeg';
		if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
		$filename = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUpload']['name']);
	
		$uploadedFile = $uploadDir . $filename;
		if (!move_uploaded_file($_FILES['fileUpload']['tmp_name'], $uploadedFile)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	
	
		//} else if($_POST['tipoUpload'] == 1){
	} else {
		$fileUrl = $googleApiUrl;
	}
	
	
	if ($isBase64EncodedImageDepois) {
		$extension = substr( $_POST['nomeArquivoDepois'], 11, 3 );
		if ($extension == 'jpe') $extension = 'jpeg';
		if (!in_array($extension,$allowedFileExtensions)) echo('BASE64: O arquivo precisa ser uma imagem');
	
		$base64str = str_replace('base64,','',stristr($_POST['nomeArquivoDepois'],';base64,'));
		$imageData = base64_decode($base64str);
	
		$filename = $_POST['usuario'] . '_' . time() . '.' . $extension;
	
		$uploadedFile = $uploadDir . $filename;
		if (!file_put_contents($uploadedFile, $imageData)) echo('BASE64: Ocorreu um erro ao salvar a imagem.');
	
	
	} else if($isUploadedImageDepois) {
		$extension = substr( $_FILES['fileUploadDepois']['type'] , -3 );
		if ($extension == 'peg') $extension = 'jpeg';
		if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
		$filenameDepois = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUploadDepois']['name']);
	
		$uploadedFile = $uploadDir . $filenameDepois;
		if (!move_uploaded_file($_FILES['fileUploadDepois']['tmp_name'], $uploadedFile)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	
	
		//} else if($_POST['tipoUpload'] == 1){
	} else {
		$fileUrlDepois = $googleApiUrl;
	}
	
	if ($isUploadedImagePrincipal) {
		$extension = substr( $_FILES['fileUploadPrincipal']['type'] , -3 );
		if ($extension == 'peg') $extension = 'jpeg';
		if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
		$filenamePrincipal = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUploadPrincipal']['name']);
	
		$uploadedFilePrinc = $uploadDir . $filenamePrincipal;
		if (!move_uploaded_file($_FILES['fileUploadPrincipal']['tmp_name'], $uploadedFilePrinc)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	}
	
	
	if ($fileUrl == $googleApiUrl) {
	
		$fov = '90';
		$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=40&pitch=0&sensor=false";
		$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=130&pitch=0&sensor=false";
		$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=220&pitch=0&sensor=false";
		$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=310&pitch=0&sensor=false";
	
		$imgBuf = array ();
		foreach ($src as $link)
		{
			$iTmp = imagecreatefromjpeg($link);
			array_push ($imgBuf,$iTmp);
		}
	
		$filename = $_POST['usuario'] . '_' . time() . '_' . 'GoogleStreetView.jpg';
		$uploadedFile = $uploadDir . $filename;
	
		$iOut = imagecreatetruecolor ("380","258") ;
		imagecopy ($iOut,$imgBuf[0],0,0,0,0,imagesx($imgBuf[0]),imagesy($imgBuf[0]));
		imagedestroy ($imgBuf[0]);
		imagecopy ($iOut,$imgBuf[1],190,0,0,0,imagesx($imgBuf[1]),imagesy($imgBuf[1]));
		imagedestroy ($imgBuf[1]);
		imagecopy ($iOut,$imgBuf[2],0,129,0,0,imagesx($imgBuf[2]),imagesy($imgBuf[2]));
		imagedestroy ($imgBuf[2]);
		imagecopy ($iOut,$imgBuf[3],190,129,0,0,imagesx($imgBuf[3]),imagesy($imgBuf[3]));
		imagedestroy ($imgBuf[3]);
		imagejpeg($iOut,$uploadedFile, '85');
		//file_put_contents($uploadedFile, file_get_contents($googleApiUrl));
	}
	
	
	$fileUrl = $appBaseUrl . 'imgs/uploaded/' . $filename;
	$fileUrlDepois = $appBaseUrl . 'imgs/uploaded/' . $filenameDepois;
	if($filenamePrincipal){
		$fileUrlPrincipal = $appBaseUrl . 'imgs/uploaded/' . $filenamePrincipal;
	}
	
	if($_FILES['fileUploadDepois'] && !$_FILES['fileUpload'])
		$fileUrl = '';*/
	
	//Agora que est� tudo validado, vamos salvar no banco:
	
	/*if($_POST['reclamacaoId']){
		$q = Doctrine_Query::create()
		->from('Reclamacoes')
		->where('id = ?', $_POST['reclamacaoId']);
	
		$reclamacao = $q->fetchOne();
	} else {*/
		$reclamacao = new Reclamacoes();
	//}
	
	 if($_POST['data_realizacao']){
		 $data_realizacao = explode("/",$_POST['data_realizacao']);
		 $data_realizacao = $data_realizacao[2]."-".$data_realizacao[1]."-".$data_realizacao[0];
		 $reclamacao->data_realizacao = $data_realizacao;
	 }
	
	$reclamacao->latitude = $_POST['latitude'];
	$reclamacao->longitude = $_POST['longitude'];
	$reclamacao->endereco = $_POST['endereco'];
	$reclamacao->titulo = $_POST['titulo'];
	$reclamacao->estado_conquista = $_POST['estado_conquista'];
	$reclamacao->descricao = $_POST['descricao'];
	$reclamacao->categoria = $_POST['categoria'];
	
	/*if ( $_POST['tipoUpload'] == 2  && $isYoutubeUrl ) { // video
		$reclamacao->ilustracao_url = $_POST['youtube_url'];
		$reclamacao->ilustracao_tipo = 'video';
	} else { // foto
		$reclamacao->ilustracao_tipo = 'foto';
	
		if($_POST['reclamacaoId']){	// edi��o
			if($_FILES['fileUpload']){ // somente atualiza se foi enviado novo arquivo, sen�o vai ficar o Google Maps
				$reclamacao->ilustracao_url = $fileUrl;
			}
	
			if($filenameDepois)
				$reclamacao->ilustracao_url_depois = $fileUrlDepois;
	
		} else {
			$reclamacao->ilustracao_url = $fileUrl;
			if($filenameDepois)
				$reclamacao->ilustracao_url_depois = $fileUrlDepois;
		}
	}*/
	
	/*if($fileUrlPrincipal){
		// File and new size
		$filename = $uploadedFilePrinc;
	
		// Get new sizes
		list($width, $height) = getimagesize($filename);
		if(strstr($_SERVER['PHP_SELF'],'cdhu') == true ){
			$newwidth = 11;
			$newheight = 35;
		} else {
			$newwidth = 24;
			$newheight = 24;
		}
		// Load
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($filename);
	
		// Resize
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	
		// Output
		imagejpeg($thumb,$uploadedFilePrinc, '85');
	
		$reclamacao->ilustracao_url_principal = $fileUrlPrincipal;
	}
	
	if(!$_POST['reclamacaoId']){
		$reclamacao->usuario_id = $_POST['usuario'];
	}*/
	
	$reclamacao->usuario_id = $_POST['usuario'];
	
	if ($appMode == 2){
		$reclamacao->aprovada = 1;
		$objeto = "conquista";
	} elseif(!$_POST['reclamacaoId']){
		$reclamacao->aprovada = 0;
		$objeto = "reclamação";
	}
	
	$reclamacao->save();
	
	echo "A sua reclamação $i foi salva com sucesso!<br>";
	
}

die();

