<?php
session_start();


// ini_set( 'display_errors', 1 ) ;
// ini_set( 'display_startup_errors', 1 ) ;
// error_reporting( E_ALL ) ;

/* approval */
// require 'user.php';

// echo "<pre>" ; print_r( $_SESSION ) ; echo "</pre>" ;


require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/Database.php';
require_once $appName.'/config/App.php';

/* DOCTRINE ***************************************************************/

spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');

$u = Doctrine_Query::create()
        ->from('Usuarios')
        ->where('facebook_id = ?', $_SESSION['fb_id']);

$Usuario = $u->fetchOne();



if(!isset($_GET['counter']))
	$counter = 1;
else
	$counter = $_GET['counter'] + 1;


$reTry = true;


if($counter > 5)
	$reTry = false;
else
	$reTry = true;

// if(!$user && !isset($approvalLogin)) {
	// $approvalLogin =  $facebook->getLoginUrl(array('scope'=>$fbPermissions,'redirect_uri'=>$appBaseUrl.'approval.php?counter='.$counter.'&'.$aprovada));
// }

// if(!$user && $reTry) {
	// header('Location: '.$approvalLogin);
// }

// if (!$user) die("Ocorreu um erro ao tentar identificar seu usu&aacute;rio do facebook. Se voce estiver logado no face e o problema persistir, possivelmente deve ser conex&atilde;o lenta entre seu computador e o facebook. Saia do approval e retorne para pagina.");

if (!$Usuario) die("Ocorreu um erro ao tentar identificar seu usu&aacute;rio.");

// if ($_REQUEST['super'] === "10c@rv@lh05") $Usuario->is_admin = true;

if ( !$Usuario->is_admin ) die('Voc&ecirc; n&atilde;o possui autoriza&ccedil;&atilde;o para ver essa p&aacute;gina.');



if(isset($_GET['user_fbid'])){
	$q = Doctrine_Query::create()
	->from('Usuarios')
	->where('facebook_id = ?', $_GET['user_fbid']);
	 
	$_user = $q->fetchOne();
	
	if(!$_user || ! isset($_user->facebook_id)){
		echo "usuario para selecionado não encontrado<br>";
		echo "<a href=\"administradores.php\">Clique Aqui para Voltar</a>";
		die();
	}
	
	if( isset( $_GET['delete'] ) ) {
		
		$_user->delete();
		echo "Usuario ".$_user->nome." foi excluido com sucesso.<br>";
	
	} else {
	
		if( !isset( $_GET['admin'] ) ) {
			echo "tipo de ação não informado.<br>";
			echo "<a href=\"administradores.php\">Clique Aqui para Voltar</a>";
			die();
		}
	
		
		$_user->is_admin = $_GET['admin'];
		$_user->save();
		
		echo "<div class=\"modal\">";
		echo "<p>Usuario ".$_user->nome." alterado com sucesso.</p>";
		echo "</div>";
		
		
		
	}
	
}
	
$q = Doctrine_Query::create()
        ->from('Usuarios')->orderBy('is_admin DESC');
$Usuarios = $q->execute();

if (empty($headerImg)) $headerImg = 'header.png';

if(! isset($HeaderDivStyle))
	$HeaderDivStyle = 'center head-header';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../style.css" />
    <script src="js/jquery_latest.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
	
	<?=$appStyle?>
    </style>

  </head>
  <body> 


  <header>
	
	    <div class="<?php echo $HeaderDivStyle;?>">
		<?php if(isset($headerImg) && $headerImg != 'none') {?>
		<a href="<?=$headerUrl?>" target="_top">
		<img src="imgs/<?php echo $headerImg; ?>" ></a>
		<?php } ?>
		</div>
	
    </header>
<br><br>


<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?=$facebookAppConfig["appId"];?>', // App ID
      channelUrl : '//campanhadigital.net.br/aplicativos/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>

  <table border="0" align="center">
  		<tr><td>
		<a href="approval.php?aprovada=0"><img src="../imgs/reclamacoespendentes.png" border="0"></a><br>
		</td>
		<td>
		<a href="approval.php?aprovada=1"><img src="../imgs/reclamacoespublicadas.png" border="0"></a><br>
		</td>
		<td>
		<a href="administradores.php"><img src="../imgs/administradoresbotao.png" border="0"></a><br>
		</td>
		</tr>
		
		
		<tr><td >
		<a href="approval.php?aprovada=2"><img src="../imgs/reclamacoesrejeitadas.png" border="0"></a><br>
		</td><td colspan="2">
		<a href="approval_comments.php"><img src="../imgs/vercomentarios.png" border="0"></a><br>
		</td></tr>
		 </table> 
  		<h1>Administradores</h1>
  		
		<br>
		
		<?php 
		if (count($Usuarios) == 0) {
			echo "<strong>Não existem Usuários</strong>";
		} else { ?>
		
		<table class="approval" border="0" align="center">
		<tr>
			<th>
			Usuário:
			</th>
			<th>
			Nome:
			</th>
			<th>
			Email:
			</th>
			<th>
			Administrador:
			</th>
			<th>
			Ação:
			</th>
		</tr>
		
		<?php
		foreach ($Usuarios as $usuario) {
		
			?>
			<tr>
				<td>
					<a href="https://facebook.com/<?=$usuario->facebook_id ?>" >
					<img src="https://graph.facebook.com/<?=$usuario->facebook_id ?>/picture" />
					</a>
				</td>
				
				<td>
					<?php echo $usuario->nome;?>
				</td>
				
				<td>
					<?php echo $usuario->email;?>
				</td>
				
				<td>
					<?php if ($usuario->is_admin) { ?>
						SIM
					<?php } else { ?>
						NÃO
					<?php } ?>
				</td>
				
				<td>
					<a style="color:#00ff00;margin-bottom:10px" href="administradores.php?user_fbid=<?=$usuario->facebook_id?>&admin=1" >Atribuir como Administrador</a> <br><br>
					
					<a style="color:#ff0000;margin-bottom:10px" href="administradores.php?user_fbid=<?=$usuario->facebook_id?>&admin=0" >Revogar Administrador</a> <br><br>
					
					<a style="color:#ff0000;margin-bottom:10px" href="administradores.php?user_fbid=<?=$usuario->facebook_id?>&delete=1" >Excluir Usuário</a> 
					
				</td>
				
			</tr>
		<?php } ?>
		</table>
		<?php } ?>
  </body>
</html>
