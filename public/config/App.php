<?php
$cats = array('saude'=>'Saúde',
		'educacao'=>'Educação',			  
		'lazer'=>'Lazer',
		'emprego'=>'Emprego', 
		'seguranca'=>'Segurança', 
		'acessibilidade'=>'Acessibilidade', 
		'habitacao'=>'Habitação',
		'obras'=>'Obras',
		'manutencao'=>'Manutenção',
		'cultura'=>'Cultura', 
		'meio_ambiente'=>'Meio Ambiente', 
        'transporte'=>'Transporte', 
        'esportes'=>'Esportes',
		'social'=>'Social',
		'participacao_social'=>'Participação Social',
		'secretarias'=>'Secretarias',
		'eventos'=>'Eventos',
		'cidade_saudavel'=>'Cidade Saudável',
		'escola_interativa'=>'Escola Interativa',
		'prefeitura_no_bairro'=>'Prefeitura no Bairro',
		'outros'=>'Outros');

$listIcons = '	.sbOptions li:nth-child(1) { display:none;  }
		.sbOptions li:nth-child(2) a { background: url("../imgs/icon_saude.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(3) a{ background: url("../imgs/icon_educacao.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(4) a{ background: url("../imgs/icon_lazer.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(5) a{ background: url("../imgs/icon_emprego.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(6) a{ background: url("../imgs/icon_seguranca.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(7) a{ background: url("../imgs/icon_acessibilidade.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(8) a{ background: url("../imgs/icon_habitacao.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(9) a{ background: url("../imgs/icon_obras.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(10) a{ background: url("../imgs/icon_manutencao.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(11) a{ background: url("../imgs/icon_cultura.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(12) a{ background: url("../imgs/icon_meio_ambiente.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(13) a{ background: url("../imgs/icon_transporte.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(14) a{ background: url("../imgs/icon_esporte.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(15) a{ background: url("../imgs/icon_social.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(16) a{ background: url("../imgs/icon_participacao_social.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(17) a{ background: url("../imgs/icon_secretarias.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(18) a{ background: url("../imgs/icon_eventos.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(19) a { background: url("../imgs/icon_cidade_saudavel.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(20) a{ background: url("../imgs/icon_escola_interativa.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(21) a{ background: url("../imgs/icon_prefeitura_no_bairro.png") no-repeat scroll 10px center transparent;  }
		.sbOptions li:nth-child(22) a{ background: url("../imgs/icon_outros.png") no-repeat scroll 10px center transparent;  }';

$comentBox = true;