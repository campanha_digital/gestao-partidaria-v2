<?php
//
/* place */
// require_once("bootstrap.php");

/*$url = $headerUrl.'?app_data=' . $_REQUEST['id'];

if ( !$_REQUEST['ajax'] ) {
	//header("Location: $url");
}*/


// begin: ajuste 04042017
require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName . '/config/App.php';
require_once $appName . '/config/Database.php';
/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();
$comentBox = 1;

try {
	$conn = Doctrine_Manager::connection($connectionUrl);

	$manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE);
	$manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

	$profiler = new Doctrine_Connection_Profiler();
	$manager->setListener($profiler);
} catch (Doctrine_Manager_Exception $e) {
	print $e->getMessage();
}

Doctrine_Core::loadModels('models');
/* DOCTRINE ***************************************************************/
// end: ajuste 04042017






$_img = "";

if (!$user) $user = $_GET['user'];

if (!empty($_GET['id'])) {
	$q = Doctrine_Query::create()
		->from('Reclamacoes')
		->where('aprovada = "1" AND id = ?', $_GET['id']);

	$Reclamacao = $q->fetchOne();
} else {
	die('Objeto não encontrado');
}

//Votos
$q = Doctrine_Query::create()
	->from('Votos')
	->where('reclamacao_id = ?', $Reclamacao->id);

$Votos = $q->execute();

$votosCount = count($Votos);
$podeVotar = '1';
foreach ($Votos as $voto) {
	if ($voto->usuario_id == $user) $podeVotar = '0';
}
// if (!$user) $podeVotar = '0';

//Comments
$q = Doctrine_Query::create()
	->from('Comentarios')
	->where('reclamacao_id = ?', $Reclamacao->id)
	->andWhere('aprovado = ?', 1);

$Comentarios = $q->execute();

//var_dump("<pre>", $q->getSqlQuery(), "</pre>");

$img = $Reclamacao->ilustracao_url;
if ($Reclamacao->ilustracao_url_depois) {
	$img_depois = $Reclamacao->ilustracao_url_depois;
}

//if($img == '')
//	$img = $img_depois;
$video = "";

if ($Reclamacao->ilustracao_tipo == 'video') {

	$lastpart = strstr($img, '&');
	$videoId = str_replace(array('http://www.youtube.com/watch?v=', 'https://www.youtube.com/watch?v=', $lastpart), '', $img);

	$img = 'https://i.ytimg.com/vi/' . $videoId . '/hqdefault.jpg';
	$videoEmbed = 'https://www.youtube.com/embed/' . $videoId;
	$videoTag = '<meta property="og:video"  content="' . $img . '" />';
}

if ($appMode == 2) {
	$textoAcao = 'Curtir';
	$textoVoto = 'curtir';
} else {
	$textoAcao = 'Apoiar';
	$textoVoto = 'apoios';
}

$voteButton = "";
if ($podeVotar == '1') {
	$voteButton = "<span class='button-verde' id='votarmodal" . $Reclamacao['id'] . "' style='margin-right:5px' onclick='javascript:" .
		"globalVotos[" . $Reclamacao['id'] . "] = " . ($votosCount + 1) . ";" .
		"globalPodeVotar[" . $Reclamacao['id'] . "] = 0;" .
		"$(\"#votosmodal" . $Reclamacao['id'] . "\").html(\"" . ($votosCount + 1) . "\");" .
		"$(\"#votos" . $Reclamacao['id'] . "\").html(\"" . ($votosCount + 1) . "\");" .
		"$.getJSON(\"vote.php\"," .
		// "{ \"usuario_id\":\"".$Reclamacao['id']."\", \"reclamacao_id\":\"".$Reclamacao['id']."\" },".
		"{ \"usuario_id\":\"" . $user . "\", \"reclamacao_id\":\"" . $Reclamacao['id'] . "\" }," .
		"     function(data){" .
		"});' >" . $textoAcao . "</span>";
} else {
	// $voteButton = "<span class='button-verde' id='votarmodal".$Reclamacao['id']."' title='Você já concordou com esta reclamação' style='margin-right:5px;background:#ccc;color:#eee' onclick=\"loginFB(true);\" >".$textoAcao."</span>";
	$voteButton = "<span class='button-verde' id='votarmodal" . $Reclamacao['id'] . "' title='Você já concordou com esta reclamação' style='margin-right:5px;background:#ccc;color:#eee' onclick=\"#\" >" . $textoAcao . "</span>";
}

if (empty($appMode)) $appMode = 0;

$showform = 1;

if ($appMode == 1) {
	if (!$Usuario->is_admin) $showform = false;
}

if ($Reclamacao->ilustracao_url_principal) {
	$imgUser = $Reclamacao->ilustracao_url_principal;
} else {
	$imgUser = 'https://graph.facebook.com/' . $Reclamacao->usuario_id . '/picture';
}
if (empty($_GET['ajax'])) {
	?>
	<!doctype html>
	<html>

	<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# <?= $appNamespace ?>: http://ogp.me/ns/fb/<?= $appNamespace ?>#">
		<meta charset="utf-8" />
		<meta property="og:locale" content="pt_BR" />
		<meta property="fb:app_id" content="<?= $facebookAppConfig['appId']; ?>" />
		<meta property="place:location:latitude" content="<?= $Reclamacao->latitude ?>">
		<meta property="place:location:longitude" content="<?= $Reclamacao->longitude ?>">
		<meta property="og:url" content="<?= $appBaseUrl . 'place.php?id=' . $Reclamacao->id ?>" />
		<meta property="og:title" content="<?= $Reclamacao->titulo ?>" />
		<meta property="og:description" content="<?= $Reclamacao->descricao ?>" />
		<meta property="og:image" content="<?= $img ?>" />
		<?= $videoTag ?>
		<link rel="stylesheet" href="../style.css" />
		<link rel="stylesheet" type="text/css" href="../scrollbars.css" />
		<link rel="stylesheet" type="text/css" href="../scrollbars-black.css" />
		<script src="../js/jquery_latest.js"></script>
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-33656101-5']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();
		</script>

	</head>

	<body>

		<div id="fb-root"></div>
		<script>
			window.fbAsyncInit = function() {
				FB.init({
					appId: '<?= $facebookAppConfig["appId"]; ?>', // App ID
					channelUrl: '//campanhadigital.net.br/aplicativos/channel.html', // Channel File
					status: true, // check login status
					cookie: true, // enable cookies to allow the server to access the session
					xfbml: true // parse XFBML
				});

				// Additional initialization code here
			};

			// Load the SDK Asynchronously
			(function(d) {
				var js, id = 'facebook-jssdk',
					ref = d.getElementsByTagName('script')[0];
				if (d.getElementById(id)) {
					return;
				}
				js = d.createElement('script');
				js.id = id;
				js.async = true;
				js.src = "//connect.facebook.net/en_US/all.js";
				ref.parentNode.insertBefore(js, ref);
			}(document));
		</script>

		<div id='mask' class='close-modal' style='display:block' onclick='javascript:window.location.href = "<?= $appBaseUrl ?>";'>
		</div>


		<!-- MODAL DO RANKING -->
		<div id="reclamacoes-container" class="modal-window amplia" style='display:block'>

		<?php } ?>



		<script>
			/*var width=$(window).width();
					var left= width - 851;
					if(left < 0)
						left = 0;
					$('#reclamacoes-container').css({left: (left)/2});*/
		</script>
		<div class="uploadedContent">
			<div class="resumo" id="resumoPlace">
				<div class="resumo">
					<h2 style="padding-bottom:5px;padding-top:5px;"><?= $cats[$Reclamacao->categoria] ?></h2>
					<div class="conteudo">
						<p><b>Nome:</b> <?= $Reclamacao->titulo ?></p>
						<p><b>Endereço:</b> <?= $Reclamacao->endereco ?></p>
						<p><b>Telefone:</b> <?= $Reclamacao->telefone ?></p>
						<p><b>E-Mail:</b> <?= $Reclamacao->email ?></p>
						<br />
						<p id="popup-descricao"><?= $Reclamacao->descricao ?></p>
					</div>
				</div>
				<div class="buttons" id="popup">
					<?php
					if ($Reclamacao->urlFacebook != null)
						echo "<a href='$Reclamacao->urlFacebook' target='_blank'><img src='../imgs/_icon_facebook.png' style='padding-left:3px' border='0' /></a>";

					if ($Reclamacao->urlTwitter != null)
						echo "<a href='$Reclamacao->urlTwitter' target='_blank'><img src='../imgs/_icon_twitter.png' style='padding-left:3px' border='0' /></a>";

					if ($Reclamacao->urlInstagram != null)
						echo "<a href='$Reclamacao->urlInstagram' target='_blank'><img src='../imgs/_icon_instagram.png' style='padding-left:3px' border='0' /></a>";

					if ($Reclamacao->urlWhatsapp != null)
						echo "<a href='$Reclamacao->urlWhatsapp' target='_blank'><img src='../imgs/_icon_whatsapp.png' style='padding-left:3px' border='0' /></a>";
					?>


				</div>
			</div>
		</div>
	</div>
	</div>

	<?php if ($comentBox === false) {

		// echo "<pre>" ;
		// print_r( get_defined_vars( ) ) ;

		?>
		<!-- CAIXA DE COMENTARIOS DA MODAL -->
		<div id="contentR">
			<div id="comentarios" style="overflow: scroll;height: 73%;">

				<ul id="modal-comments" class="comments">
					<?php
					foreach ($Comentarios as $comentario) { ?>

						<li>
							<div>
								<a target="_blank" href="https://www.facebook.com/<?php echo $comentario->usuario_id; ?>">
									<img src="https://graph.facebook.com/<?= $comentario->usuario_id ?>/picture" width="50" height="50">
								</a>
								<p><?= $comentario->texto ?></p>
							</div>
						</li>

					<?php } ?>
				</ul>
			</div>
			<div id="makeComent">
				<ul class="comments">
					<li>

						<?php if ($user) { ?>
							<div style="padding-bottom:9px;">
								<img id="usr-picture" src="https://graph.facebook.com/<?= $user ?>/picture" width="50" height="50">
								<textarea id="modal-comment-box" style="width: 70%; height: 50px;margin: 0 0 0 10px;" original="Escreva aqui seu comentário" onFocus="if ($(this).val() == $(this).attr('original')) $(this).val('');" onBlur="if ($(this).val() == '') $(this).val( $(this).attr('original') );">Escreva aqui seu comentário</textarea>
							</div>
							<div class="buttons" style="padding:0">
								<span id="comment" class="button-verde" onClick="var mycomment = $('#modal-comment-box').val();commentOnAction(mycomment);">Comentar</span>
							</div>

						<?php } else { ?>

							<div class="buttons" style="background:#f0f0f0;padding-top:35px;text-align:center;width:100%;height:40px">
								<span class='button-verde' id='comment' title='Você deve estar logado no facebook...' style='margin-right:5px;background:#ccc;color:#eee' onClick="loginFB(true);">Comentar</span>";
							</div>
						<?php } ?>

					</li>
				</ul>
			</div>
		</div>
	<? } ?>

	<?php

	$_img = $img;
	if (empty($_img)) {

		$_img = $img_depois;
	}

	?>

	<script>
		$(document).ready(function() {

			$('span[id=share]').bind('click', function() {

				postOnFacebook('<?= $appPostMessage ?>', '<?= $Reclamacao->titulo ?>', '<?= $Reclamacao->endereco ?>', '<?= str_replace("\r", "", str_replace("\n", "", $Reclamacao->descricao)); ?>', '<?= $headerUrl . "?app_data=" . $Reclamacao->id ?>', '<?= $_img ?>', '<?= $appSharePrompt ?>');

				//postToWallUsingFBApi();

			});

			<?php if (!empty($_GET['sharenow'])) { ?>

				$('span[id=share]').trigger('click');

			<?php } ?>

		});

		function postToWallUsingFBApi() {
			FB.api('https://<?= $Reclamacao->facebook_post_id ?>/me/feed', 'post', {
					'message': 'teste de publicação'
				},
				function(response) {
					if (!response || response.error) {
						console.log(response);
					} else {
						console.log('ID: ' + response.id);
					}
				}
			);
		}

		function commentOnAction(commentmessage) {

			$('#modal-comments').load('comments.php', {
				'id': '<?= $Reclamacao->id ?>',
				'user': '<?= $user ?>',
				'texto': commentmessage
			});

			FB.api('https://<?= $Reclamacao->facebook_post_id ?>/comments', 'post', {
					'message': commentmessage
				},
				function(response) {
					if (!response || response.error) {
						//console.log(response.error);
					} else {
						//console.log('Action was successful! Action ID: ' + response.id);
					}
				});
		}
	</script>


	<?php
	if (empty($_GET['ajax'])) { ?>
		</div>

		<script type="text/javascritpt">
			function killButton(buttonId){
			$('#'+buttonId).click( function(){return false;} );
			$('#'+buttonId).attr( 'onclick' , 'return false;' );
			$('#'+buttonId).css('background','#ccc');
			$('#'+buttonId).css('color','#eee');
		}

	</script>
	</body>

	</html>
<?php } ?>