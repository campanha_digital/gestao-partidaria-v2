<?php
/* comments */
// require_once("bootstrap.php");



// begin: ajuste 04042017
require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/Database.php';
/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');
/* DOCTRINE ***************************************************************/
// end: ajuste 04042017




if (!$user) $user = $_POST['user'];

if ( !empty($_POST['id']) ) {
	$q = Doctrine_Query::create()
	        ->from('Reclamacoes')
	        ->where('id = ?', $_POST['id']);
	        
	$Reclamacao = $q->fetchOne();
	
} else { die('Reclamação não encontrada'); }


if ($user && !empty($_POST['texto'])) {

	$Comentario = new Comentarios();
	$Comentario->usuario_id = $user;
	$Comentario->reclamacao_id = $Reclamacao->id;
	$Comentario->texto = $_POST['texto'];
	$Comentario->save();
	
	?>
	<script>
	$('#modal-comment-box').val('');
	alert('Comentario enviado. Obrigado.');
	</script>
	<?php 

}

$q = Doctrine_Query::create()
        ->from('Comentarios')
        ->where('reclamacao_id = ?', $Reclamacao->id)
        ->where('aprovado = ?','1');

$Comentarios = $q->execute();

foreach ($Comentarios as $comentario) { ?>
	                        
	<li>
		<div>
		    <img src="https://graph.facebook.com/<?=$comentario->usuario_id?>/picture" width="50" height="50"> 
		    <p><?=$comentario->texto?></p>
		</div>
	</li>
	                        
<?php } ?>