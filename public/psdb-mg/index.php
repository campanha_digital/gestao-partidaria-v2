<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
session_start();

// begin: ajuste tsunami 03042017
if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['code'])){
	$appId = '217700365840618';
	$appSecret = 'c3b1ba8e1cdcbee4a06be84d44a43b5d';

	if ($_SERVER['HTTP_HOST'] == 'localhost') { // Development
		$redirectUri = urlencode('http://localhost/psdb-mg/?facebookLogin=true');
	} else {
		$redirectUri = urlencode('https://campanhadigital.net.br/sgp/psdb-mg/?facebookLogin=true');
	}

	$code = $_GET['code'];

	$token_url = "https://graph.facebook.com/oauth/access_token?"
		. "client_id=" . $appId 
		. "&redirect_uri=" . $redirectUri
		. "&client_secret=" . $appSecret 
		. "&code=" . $code;

	$response = @file_get_contents($token_url);
		if($response){
			$params = json_decode( $response ) ;

			if(isset($params->access_token)){
				$graph_url = "https://graph.facebook.com/me?access_token=" 
					. $params->access_token 
					. "&fields=email,name,about,feed,locale,link,hometown,location,political,religion,likes" ;

				$fbuser = json_decode(file_get_contents($graph_url));
				
				$_USER_FACE_LOGIN = true;
				$_SESSION['fb_id'] = $fbuser->id;
				$_SESSION['fb_name'] = $fbuser->name;
				$_SESSION['fb_email'] = $fbuser->email;
				$_SESSION['fb_link'] = $fbuser->link;
				$_SESSION['fb_token'] = $params->access_token ;
				$_SESSION['fb_object'] = serialize($facebook);
				// $_SESSION['fb_user_profile'] = $fbuser->link;
				$_SESSION['appNameSpace'] = $appNamespace;
				
				

			} else {
				error_log( "Erro de conexão com Facebook1 : token não existe" );
			}

		}else{
			error_log( "Erro de conexão com Facebook2 : response não existe" );
		}
}else if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['error'])){
	error_log ( 'Permissão não concedida' );
}

// echo "<pre>" ; print_r( $_SESSION ) ; echo "</pre>" ;
// end: ajuste tsunami 03042017



if(!isset($_REQUEST['signed_request']) && isset($_REQUEST['pagetab']))
	header('Location : '.$appBaseUrl);


$appName = getcwd();
$app_arr = explode(DIRECTORY_SEPARATOR, $appName);
$appName = $app_arr[count($app_arr) - 1];
$rootDir = str_replace($appName, '', dirname($_SERVER['SCRIPT_FILENAME']));

chdir($rootDir);
include('index.php');


function getCurrentPageURL() {
    $pageURL = 'http';

    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";

    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }

    return $pageURL;
}
