<?php
/* logout */

session_start();

if(isset($_SESSION['fb_id']) && $_SESSION['fb_id'] != '') {
	$user = unserialize($_SESSION['fb_id']);
	$facebook = unserialize($_SESSION['fb_object']);
// 	setcookie('fbs_'.$facebook->getAppId(), '', time()-100, '/', 'campanhadigital.net.br');
	$FROM_LOGOUT = true;
	$_SESSION['fb_id'] = NULL;
	$_SESSION['fb_object'] = NULL;
	unset($_SESSION['fb_id']);
	unset($_SESSION['fb_object']);
	unset($_SESSION);
	session_unset();
// 	require_once 'bootstrap.php';
	
	$_SESSION['fb_id'] = NULL;
	$_SESSION['fb_object'] = NULL;
	unset($_SESSION['fb_id']);
	unset($_SESSION['fb_object']);
	unset($_SESSION);
	session_unset();
} else {
	require_once $appName.'/config/App.php';
}

$_SESSION['appNameSpace'] = NULL;
unset($_SESSION['appNameSpace']);
$_SESSION['fb_id'] = NULL;
$_SESSION['fb_object'] = NULL;
unset($_SESSION['fb_id']);
unset($_SESSION['fb_object']);
unset($_SESSION);
session_unset();
session_destroy();
header('Location: '.$appBaseUrl.'?counter='.$counter);
