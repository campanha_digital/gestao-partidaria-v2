<?php
// ini_set( 'display_errors', 1 ) ;
// ini_set( 'display_startup_errors', 1 ) ;
// error_reporting( E_ALL ) ;
// echo "<pre>" ; print_r( 'upload.php' ) ; echo "</pre>" ;
// echo "<pre>" ; print_r( $uploadDir ) ; echo "</pre>" ;
// echo "<pre>" ; print_r( $_POST ) ; echo "</pre>" ;

// require_once "bootstrap.php";
// require_once "user.php";
require_once "functions.php";
require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/Database.php';
require_once $appName.'/config/App.php';

$uploadDir = $appName . '/imgs/uploaded/' ;



/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');
/* DOCTRINE ***************************************************************/


//Valida��es B�sicas
if 
(
    empty($_POST['usuario'])    || 
    empty($_POST['endereco'])   || 
    empty($_POST['latitude'])   || 
    empty($_POST['longitude'])  || 
    empty($_POST['categoria'])  || 
    empty($_POST['titulo'])     || 
    empty($_POST['descricao'])
)
echo("Dados obrigat�rios faltando");

$googleApiUrl = "https://maps.googleapis.com/maps/api/streetview?size=381x257&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=90&heading=235&pitch=10&sensor=false";
$isYoutubeUrl = ( !empty($_POST['youtube_url']) && ( strpos($_POST['youtube_url'], 'youtube.com') !== false ) );

if ( $_POST['tipoUpload'] == 1  && empty($_POST['nomeArquivo']) ) $fileUrl = $googleApiUrl;
if ( $_POST['tipoUpload'] == 2  && !$isYoutubeUrl ) $fileUrl = $googleApiUrl;

if ( $_POST['tipoUploadDepois'] == 1  && empty($_POST['nomeArquivoDepois']) ) $fileUrlDepois = $googleApiUrl;
if ( $_POST['tipoUploadDepois'] == 2  && !$isYoutubeUrl ) $fileUrlDepois = $googleApiUrl;

$isBase64EncodedImage = ( empty($_FILES) && substr($_POST['nomeArquivo'],0,11) == 'data:image/' );
$isBase64EncodedImageDepois = ( empty($_FILES) && substr($_POST['nomeArquivoDepois'],0,11) == 'data:image/' );
$isBase64EncodedImagePrincipal = ( empty($_FILES) && substr($_POST['nomeArquivoPrincipal'],0,11) == 'data:image/' );
$isUploadedImage      = (!empty($_FILES) && substr($_FILES['fileUpload']['type'],0,6) == 'image/');
$isUploadedImageDepois      = (!empty($_FILES) && substr($_FILES['fileUploadDepois']['type'],0,6) == 'image/');
$isUploadedImagePrincipal      = (!empty($_FILES) && substr($_FILES['fileUploadPrincipal']['type'],0,6) == 'image/');

if ($isBase64EncodedImage) {
	$extension = substr( $_POST['nomeArquivo'], 11, 3 );
	if ($extension == 'jpe') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('BASE64: O arquivo precisa ser uma imagem'); 
	
	$base64str = str_replace('base64,','',stristr($_POST['nomeArquivo'],';base64,'));
	$imageData = base64_decode($base64str);
	
	$filename = $_POST['usuario'] . '_' . time() . '.' . $extension;
	
	$uploadedFile = $uploadDir . $filename;
	if (!file_put_contents($uploadedFile, $imageData)) echo('BASE64: Ocorreu um erro ao salvar a imagem.');
	
	
} else if($isUploadedImage) {
	$extension = substr( $_FILES['fileUpload']['type'] , -3 ); 
	if ($extension == 'peg') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
	$filename = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUpload']['name']);
	$filename = str_replace( '.'.$extension, "", $filename );
	$filename = replace_chars( $filename ) . '.' . $extension;
	
	$uploadedFile = $uploadDir . $filename;
	if (!move_uploaded_file($_FILES['fileUpload']['tmp_name'], $uploadedFile)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	
	
//} else if($_POST['tipoUpload'] == 1){
} else {
	$fileUrl = $googleApiUrl;
}


if ($isBase64EncodedImageDepois) {
	$extension = substr( $_POST['nomeArquivoDepois'], 11, 3 );
	if ($extension == 'jpe') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('BASE64: O arquivo precisa ser uma imagem'); 
	
	$base64str = str_replace('base64,','',stristr($_POST['nomeArquivoDepois'],';base64,'));
	$imageData = base64_decode($base64str);
	
	$filename = $_POST['usuario'] . '_' . time() . '.' . $extension;
	
	$uploadedFile = $uploadDir . $filename;
	if (!file_put_contents($uploadedFile, $imageData)) echo('BASE64: Ocorreu um erro ao salvar a imagem.');
	
	
} else if($isUploadedImageDepois) {
	$extension = substr( $_FILES['fileUploadDepois']['type'] , -3 ); 
	if ($extension == 'peg') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
	$filenameDepois = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUploadDepois']['name']);
	$filenameDepois = str_replace( '.'.$extension, "", $filenameDepois );
	$filenameDepois = replace_chars( $filenameDepois ) . '.' . $extension;
	
	$uploadedFile = $uploadDir . $filenameDepois;
	if (!move_uploaded_file($_FILES['fileUploadDepois']['tmp_name'], $uploadedFile)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	
	
//} else if($_POST['tipoUpload'] == 1){
} else {
	$fileUrlDepois = $googleApiUrl;
}

if ($isUploadedImagePrincipal) {
	$extension = substr( $_FILES['fileUploadPrincipal']['type'] , -3 ); 
	if ($extension == 'peg') $extension = 'png';
	if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
	$filenamePrincipal = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUploadPrincipal']['name']);
	$filenamePrincipal = str_replace( '.'.$extension, "", $filenamePrincipal );
	$filenamePrincipal = replace_chars( $filenamePrincipal ) . '.' . $extension;
	
	
	$uploadedFilePrinc = $uploadDir . $filenamePrincipal;
	if (!move_uploaded_file($_FILES['fileUploadPrincipal']['tmp_name'], $uploadedFilePrinc)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
}


if ($fileUrl == $googleApiUrl) {
	
	$fov = '90';
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=40&pitch=0&sensor=false";
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=130&pitch=0&sensor=false";
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=220&pitch=0&sensor=false";
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=310&pitch=0&sensor=false";

	$imgBuf = array ();
	foreach ($src as $link)
	{
	   $iTmp = imagecreatefromjpeg($link);
	   array_push ($imgBuf,$iTmp);
	}

	$filename = $_POST['usuario'] . '_' . time() . '_' . 'GoogleStreetView.png';
	$uploadedFile = $uploadDir . $filename;

	$iOut = imagecreatetruecolor ("380","258") ;
	imagecopy ($iOut,$imgBuf[0],0,0,0,0,imagesx($imgBuf[0]),imagesy($imgBuf[0]));
	imagedestroy ($imgBuf[0]);
	imagecopy ($iOut,$imgBuf[1],190,0,0,0,imagesx($imgBuf[1]),imagesy($imgBuf[1]));
	imagedestroy ($imgBuf[1]);
	imagecopy ($iOut,$imgBuf[2],0,129,0,0,imagesx($imgBuf[2]),imagesy($imgBuf[2]));
	imagedestroy ($imgBuf[2]);
	imagecopy ($iOut,$imgBuf[3],190,129,0,0,imagesx($imgBuf[3]),imagesy($imgBuf[3]));
	imagedestroy ($imgBuf[3]);
	imagejpeg($iOut,$uploadedFile, '85');
	//file_put_contents($uploadedFile, file_get_contents($googleApiUrl));	
}


$fileUrl = $appBaseUrl . 'imgs/uploaded/' . $filename;
$fileUrlDepois = $appBaseUrl . 'imgs/uploaded/' . $filenameDepois;
if($filenamePrincipal){
	$fileUrlPrincipal = $appBaseUrl . 'imgs/uploaded/' . $filenamePrincipal;
}

if($_FILES['fileUploadDepois'] && !$_FILES['fileUpload'])
	$fileUrl = '';

//Agora que est� tudo validado, vamos salvar no banco:

if($_POST['reclamacaoId']){
	$q = Doctrine_Query::create()
        ->from('Reclamacoes')
        ->where('id = ?', $_POST['reclamacaoId']);

	$reclamacao = $q->fetchOne();
} else {
	$reclamacao = new Reclamacoes();
}

/*if($_POST['data_realizacao']){
	$data_realizacao = explode("/",$_POST['data_realizacao']);
	$data_realizacao = $data_realizacao[2]."-".$data_realizacao[1]."-".$data_realizacao[0];
	$reclamacao->data_realizacao = $data_realizacao;
}*/

$reclamacao->latitude = $_POST['latitude'];
$reclamacao->longitude = $_POST['longitude'];
$reclamacao->endereco = $_POST['endereco'];
$reclamacao->titulo = $_POST['titulo'];
$reclamacao->estado_conquista = $_POST['estado_conquista'];
$reclamacao->descricao = nl2br($_POST['descricao']);
$reclamacao->categoria = $_POST['categoria'];
$reclamacao->telefone = $_POST['telefone'];
$reclamacao->email = $_POST['email'];
$reclamacao->urlFacebook = $_POST['urlFacebook'];
$reclamacao->urlTwitter = $_POST['urlTwitter'];
$reclamacao->urlInstagram = $_POST['urlInstagram'];
$reclamacao->urlWhatsapp = "https://web.whatsapp.com/send?phone=55" . preg_replace('/[^0-9]/', '', $_POST['urlWhatsapp']) . "&text=Contato Via Gestão Partidária";

if ( $_POST['tipoUpload'] == 2  && $isYoutubeUrl ) { // video
	$reclamacao->ilustracao_url = $_POST['youtube_url'];
	$reclamacao->ilustracao_tipo = 'video';
} else { // foto
	$reclamacao->ilustracao_tipo = 'foto';

	if($_POST['reclamacaoId']){	// edi��o
		if($_FILES['fileUpload']){ // somente atualiza se foi enviado novo arquivo, sen�o vai ficar o Google Maps
			$reclamacao->ilustracao_url = $fileUrl;
		}
		
		if($filenameDepois)
			$reclamacao->ilustracao_url_depois = $fileUrlDepois;
		
	} else {
		$reclamacao->ilustracao_url = $fileUrl;
		if($filenameDepois)
			$reclamacao->ilustracao_url_depois = $fileUrlDepois;
	}
}

if($fileUrlPrincipal){
	// File and new size
	$filename = $uploadedFilePrinc;

	// Get new sizes
	list($width, $height) = getimagesize($filename);
	if(strstr($_SERVER['PHP_SELF'],'cdhu') == true ){
    	$newwidth = 11;
		$newheight = 35;
	} else {
		$newwidth = 24;
		$newheight = 24;
	}
	// Load
	$thumb = imagecreatetruecolor($newwidth, $newheight);
	$source = imagecreatefromjpeg($filename);

	// Resize
	imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

	// Output
	imagejpeg($thumb,$uploadedFilePrinc, '85');

	$reclamacao->ilustracao_url_principal = $fileUrlPrincipal;
}

if(!$_POST['reclamacaoId']){
	$reclamacao->usuario_id = $_POST['usuario'];
}

if ($appMode == 2){
	$reclamacao->aprovada = 1;
	$objeto = "conquista";
} elseif(!$_POST['reclamacaoId']){
	$reclamacao->aprovada = 0;
	$objeto = "reclama��o";
}


if(!$_POST['reclamacaoId']){
	$mensagem = "Foi inclu�da uma nova ".$objeto.". Abaixo alguns dados:\n 
	T�tulo: ".$_POST['titulo']." \n
	Descri��o: ".$_POST['descricao']."\n
	\n
	Acesse o admin para aprovar ou reprovar a ".$objeto.".";
	
	if($emailAdmin) { 
		$headers = "MIME-Version: 1.1\n";
		$headers .= "Content-type: text/plain; charset=utf-8\n";
		$headers .= "From: atendimento@campanhadigital.net.br\n"; // remetente
		$headers .= "Return-Path: atendimento@campanhadigital.net.br\n"; // return-path
		$envio = mail($emailAdmin, utf8_encode("Nova ".$objeto." inclu�da"), $mensagem, $headers);
	}
}

$reclamacao->save();

$return['message'] = $appPostMessage;
$return['name'] = $reclamacao->titulo;
$return['caption'] = $reclamacao->endereco;
// $return['description'] = str_replace("\r", "", str_replace("\n", "", $reclamacao->descricao));
$return['description'] = $reclamacao->descricao;
$return['link'] = $facebookBaseUrl."?app_data=".$reclamacao->id;

if( $reclamacao->ilustracao_url == '') {
	$return['picture'] = $reclamacao->ilustracao_url_depois;
} else {
	$return['picture'] = $reclamacao->ilustracao_url;
}

$return['user_message'] = $appSharePrompt;

echo json_encode($return);

// echo "<pre>" ; print_r( $return ) ; echo "</pre>" ;


?>