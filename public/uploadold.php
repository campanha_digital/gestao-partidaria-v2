﻿<?php
require_once "bootstrap.php";
require_once "user.php";

//Validações Básicas
if 
(
    empty($_POST['usuario'])    || 
    empty($_POST['endereco'])   || 
    empty($_POST['latitude'])   || 
    empty($_POST['longitude'])  || 
    empty($_POST['categoria'])  || 
    empty($_POST['titulo'])     || 
    empty($_POST['descricao'])
)
echo("Dados obrigatórios faltando");

$googleApiUrl = "https://maps.googleapis.com/maps/api/streetview?size=381x257&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=90&heading=235&pitch=10&sensor=false";
$isYoutubeUrl = ( !empty($_POST['youtube_url']) && ( strpos($_POST['youtube_url'], 'youtube.com') !== false ) );

if ( $_POST['tipoUpload'] == 1  && empty($_POST['nomeArquivo']) ) $fileUrl = $googleApiUrl;
if ( $_POST['tipoUpload'] == 2  && !$isYoutubeUrl ) $fileUrl = $googleApiUrl;

if ( $_POST['tipoUploadDepois'] == 1  && empty($_POST['nomeArquivoDepois']) ) $fileUrlDepois = $googleApiUrl;
if ( $_POST['tipoUploadDepois'] == 2  && !$isYoutubeUrl ) $fileUrlDepois = $googleApiUrl;

$isBase64EncodedImage = ( empty($_FILES) && substr($_POST['nomeArquivo'],0,11) == 'data:image/' );
$isBase64EncodedImageDepois = ( empty($_FILES) && substr($_POST['nomeArquivoDepois'],0,11) == 'data:image/' );
$isUploadedImage      = (!empty($_FILES) && substr($_FILES['fileUpload']['type'],0,6) == 'image/');
$isUploadedImageDepois      = (!empty($_FILES) && substr($_FILES['fileUploadDepois']['type'],0,6) == 'image/');

if ($isBase64EncodedImage) {
	$extension = substr( $_POST['nomeArquivo'], 11, 3 );
	if ($extension == 'jpe') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('BASE64: O arquivo precisa ser uma imagem'); 
	
	$base64str = str_replace('base64,','',stristr($_POST['nomeArquivo'],';base64,'));
	$imageData = base64_decode($base64str);
	
	$filename = $_POST['usuario'] . '_' . time() . '.' . $extension;
	
	$uploadedFile = $uploadDir . $filename;
	if (!file_put_contents($uploadedFile, $imageData)) echo('BASE64: Ocorreu um erro ao salvar a imagem.');
	
	
} else if($isUploadedImage) {
	$extension = substr( $_FILES['fileUpload']['type'] , -3 ); 
	if ($extension == 'peg') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
	$filename = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUpload']['name']);
	
	$uploadedFile = $uploadDir . $filename;
	if (!move_uploaded_file($_FILES['fileUpload']['tmp_name'], $uploadedFile)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	
	
//} else if($_POST['tipoUpload'] == 1){
} else {
	$fileUrl = $googleApiUrl;
}


if ($isBase64EncodedImageDepois) {
	$extension = substr( $_POST['nomeArquivoDepois'], 11, 3 );
	if ($extension == 'jpe') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('BASE64: O arquivo precisa ser uma imagem'); 
	
	$base64str = str_replace('base64,','',stristr($_POST['nomeArquivoDepois'],';base64,'));
	$imageData = base64_decode($base64str);
	
	$filename = $_POST['usuario'] . '_' . time() . '.' . $extension;
	
	$uploadedFile = $uploadDir . $filename;
	if (!file_put_contents($uploadedFile, $imageData)) echo('BASE64: Ocorreu um erro ao salvar a imagem.');
	
	
} else if($isUploadedImage) {
	$extension = substr( $_FILES['fileUploadDepois']['type'] , -3 ); 
	if ($extension == 'peg') $extension = 'jpeg';
	if (!in_array($extension,$allowedFileExtensions)) echo('Upload: O arquivo precisa ser uma imagem');
	
	$filenameDepois = $_POST['usuario'] . '_' . time() . '_' . basename($_FILES['fileUploadDepois']['name']);
	
	$uploadedFile = $uploadDir . $filenameDepois;
	if (!move_uploaded_file($_FILES['fileUploadDepois']['tmp_name'], $uploadedFile)) echo('Upload: Ocorreu um erro ao salvar a imagem.');
	
	
//} else if($_POST['tipoUpload'] == 1){
} else {
	$fileUrlDepois = $googleApiUrl;
}



if ($fileUrl == $googleApiUrl) {
	
	$fov = '90';
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=40&pitch=0&sensor=false";
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=130&pitch=0&sensor=false";
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=220&pitch=0&sensor=false";
	$src[] = "https://maps.googleapis.com/maps/api/streetview?size=190x129&location=".$_POST['latitude'].",%20".$_POST['longitude']."&fov=".$fov."&heading=310&pitch=0&sensor=false";

	$imgBuf = array ();
	foreach ($src as $link)
	{
	   $iTmp = imagecreatefromjpeg($link);
	   array_push ($imgBuf,$iTmp);
	}

	$filename = $_POST['usuario'] . '_' . time() . '_' . 'GoogleStreetView.jpg';
	$uploadedFile = $uploadDir . $filename;

	$iOut = imagecreatetruecolor ("380","258") ;
	imagecopy ($iOut,$imgBuf[0],0,0,0,0,imagesx($imgBuf[0]),imagesy($imgBuf[0]));
	imagedestroy ($imgBuf[0]);
	imagecopy ($iOut,$imgBuf[1],190,0,0,0,imagesx($imgBuf[1]),imagesy($imgBuf[1]));
	imagedestroy ($imgBuf[1]);
	imagecopy ($iOut,$imgBuf[2],0,129,0,0,imagesx($imgBuf[2]),imagesy($imgBuf[2]));
	imagedestroy ($imgBuf[2]);
	imagecopy ($iOut,$imgBuf[3],190,129,0,0,imagesx($imgBuf[3]),imagesy($imgBuf[3]));
	imagedestroy ($imgBuf[3]);
	imagejpeg($iOut,$uploadedFile, '85');
	//file_put_contents($uploadedFile, file_get_contents($googleApiUrl));	
}


$fileUrl = $appBaseUrl . 'imgs/uploaded/' . $filename;
$fileUrlDepois = $appBaseUrl . 'imgs/uploaded/' . $filenameDepois;

//Agora que está tudo validado, vamos salvar no banco:

$reclamacao = new Reclamacoes();

$reclamacao->latitude = $_POST['latitude'];
$reclamacao->longitude = $_POST['longitude'];
$reclamacao->usuario_id = $_POST['usuario'];
$reclamacao->endereco = $_POST['endereco'];
$reclamacao->titulo = $_POST['titulo'];
$reclamacao->estado_conquista = $_POST['estado_conquista'];
$reclamacao->descricao = $_POST['descricao'];
if ( $_POST['tipoUpload'] == 2  && $isYoutubeUrl ) {
	$reclamacao->ilustracao_url = $_POST['youtube_url'];
	$reclamacao->ilustracao_tipo = 'video';
} else {
	$reclamacao->ilustracao_url = $fileUrl;
	$reclamacao->ilustracao_url_depois = $fileUrlDepois;
	$reclamacao->ilustracao_tipo = 'foto';
}
$reclamacao->categoria = $_POST['categoria'];

if($Usuario->is_admin == 1){
	$reclamacao->aprovada = 1;
} else {
	$reclamacao->aprovada = 0;
}

$reclamacao->save();

echo "A sua reclamação foi salva com sucesso!";
?>
<script>
var isMSIE = /*@cc_on!@*/0;

if (isMSIE) {
	window.location = "<?=$appBaseUrl?>";
}
</script>
