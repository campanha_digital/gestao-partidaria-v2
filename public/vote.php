<?php
/* vote */
// require_once 'bootstrap.php';


// begin: ajuste 04042017
require_once 'libraries/Doctrine-1.2.4/Doctrine.php';
require_once $appName.'/config/Database.php';
/* DOCTRINE ***************************************************************/
spl_autoload_register(array('Doctrine', 'autoload'));
spl_autoload_register(array('Doctrine_Core', 'modelsAutoload'));

$manager = Doctrine_Manager::getInstance();

try { 
  $conn = Doctrine_Manager::connection($connectionUrl);
  
  $manager->setAttribute(Doctrine_Core::ATTR_MODEL_LOADING, Doctrine_Core::MODEL_LOADING_CONSERVATIVE); 
  $manager->setAttribute(Doctrine_Core::ATTR_EXPORT, Doctrine_Core::EXPORT_ALL);

  $profiler = new Doctrine_Connection_Profiler();
  $manager->setListener($profiler);

} catch (Doctrine_Manager_Exception $e) {
  print $e->getMessage();
}

Doctrine_Core::loadModels('models');

// O usuário existe no banco?
$u = Doctrine_Query::create()
        ->from('Usuarios')
        ->where('facebook_id = ?', $_SESSION['fb_id']);

$Usuario = $u->fetchOne();

/* DOCTRINE ***************************************************************/
// end: ajuste 04042017




//@TODO precisa certificar que é o próprio usuário que está fazendo
//sem exibir o token dele no Javascript
if ( !empty($_GET['usuario_id']) && !empty($_GET['reclamacao_id'])) {
	$q = Doctrine_Query::create()
	        ->from('Reclamacoes')
	        ->where('id = ?', $_GET['reclamacao_id']);
	        
	$Reclamacao = $q->fetchOne();
	
	$q = Doctrine_Query::create()
	        ->from('Usuarios')
	        ->where('facebook_id = ?', $_GET['usuario_id']);
	        
	$Usuario = $q->fetchOne();
	
} else { die('Parâmetros incoretos'); }

$q = Doctrine_Query::create()
	->from('Votos')
	->where('id = ?', $Usuario->facebook_id . "_" . $Reclamacao->id);

$Voto = $q->fetchOne();

$return = array();

if ($Voto) {
	$return['valido'] = '0';
} else {
	$return['valido'] = '1';
	
	if (empty($_GET['consulta'])) {
		$Voto = new Votos();
		$Voto->id = $Usuario->facebook_id . "_" . $Reclamacao->id;
		$Voto->usuario_id = $Usuario->facebook_id;
		$Voto->reclamacao_id = $Reclamacao->id;
		$Voto->datetime = new Doctrine_Expression('NOW()');
		$Voto->save();
	}
}

$q = Doctrine_Query::create()
	->from('Votos')
	->where('reclamacao_id = ?', $Reclamacao->id);

$return['votos'] = count($q->execute());

echo json_encode($return);